package threads.odin.model;


import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.TypeConverter;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Info;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.data.blocks.BLOCKS;
import threads.odin.data.conns.CONNS;
import threads.odin.data.files.FILES;
import threads.odin.data.files.FileInfo;
import threads.odin.data.peers.PEERS;
import threads.odin.data.relays.RELAYS;
import threads.odin.utils.MimeTypeService;

public class API {

    public static final String IP = "IPv6";
    public static final String IDX = "idx";
    public static final String URI = "uri";
    public static final String PNS = "pns";
    public static final String URL = "url";
    public static final String FILE = "file";

    public static final int CLICK_OFFSET = 500;
    public static final Payload PNS_RECORD = new Payload(20);
    private static final String TAG = API.class.getSimpleName();
    private static final String IPFS_KEY = "IPFS_KEY";
    private static final String PRIVATE_KEY = "PRIVATE_KEY";
    private static final String PUBLIC_KEY = "PUBLIC_KEY";
    private static final String APP_KEY = "LITE_KEY";
    private static final String TITLE_KEY = "TITLE_KEY";
    private static volatile API INSTANCE = null;
    private final ReentrantLock lock = new ReentrantLock();
    private final FILES files;
    private final String host;
    private final Session session;
    private final Server server;
    @NonNull
    private final AtomicReference<Dir> homepage = new AtomicReference<>();

    private API(@NonNull Context context) throws Exception {

        Lite.Settings settings = new Lite.Settings(
                keys(context), bootstrap(), "lite/1.0.0/");

        Lite lite = new Lite(settings);
        files = FILES.getInstance(context);
        BLOCKS blocks = BLOCKS.getInstance(context);
        PEERS peers = PEERS.getInstance(context);
        session = lite.createSession(blocks, peers);
        try {
            host = lite.self().toBase36();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        CONNS conns = CONNS.getInstance(context);
        RELAYS relays = RELAYS.getInstance(context);
        server = lite.startServer(Lite.createServerSettings(5001),
                blocks,
                peers,
                conns::addConn,
                conns::removeConn,
                relays::addRelay,
                relays::removeRelay,
                peerId -> false,
                request -> createEnvelope(context, request.payload()),
                envelope -> {
                });

        initPage(context);

        try {
            MDNS mdns = MDNS.mdns(context);
            mdns.startService(server);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static void setTitle(@NonNull Context context, @NonNull String title) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(TITLE_KEY, title);
        editor.apply();
    }

    @NonNull
    public static String getTitle(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(TITLE_KEY,
                context.getString(R.string.homepage)));
    }

    public static API getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (API.class) {
                if (INSTANCE == null) {
                    INSTANCE = new API(context);
                }
            }
        }
        return INSTANCE;
    }

    private static String nameWithoutExtension(@NonNull String file) {
        String fileName = new File(file).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
    }

    private static String fileExtension(@NonNull String fullName) {
        String fileName = new File(fullName).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    @NonNull
    public static String host(@NonNull Peeraddr peeraddr) throws UnknownHostException {
        return Objects.requireNonNull(InetAddress.getByAddress(peeraddr.address()).getHostAddress());
    }

    public static String compactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    @NonNull
    public static String size(@NonNull FileInfo fileInfo) {

        String fileSize;
        long size = fileInfo.size();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize + " B";
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize + " KB";
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize + " MB";
        }
    }

    @NonNull
    public static String mimeType(@NonNull Context context, @NonNull Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        if (mimeType == null) {
            mimeType = MimeTypeService.OCTET_MIME_TYPE;
        }
        return mimeType;
    }

    @NonNull
    public static String fileName(@NonNull Context context, @NonNull Uri uri) {
        String filename = null;

        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            filename = cursor.getString(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (filename == null) {
            filename = uri.getLastPathSegment();
        }

        if (filename == null) {
            filename = "file_name_not_detected";
        }

        return filename;
    }

    public static long fileSize(@NonNull Context context, @NonNull Uri uri) {

        ContentResolver contentResolver = context.getContentResolver();

        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            return cursor.getLong(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        try (ParcelFileDescriptor fd = contentResolver.openFileDescriptor(uri, "r")) {
            Objects.requireNonNull(fd);
            return fd.getStatSize();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return -1;
    }

    @NonNull
    public static File createTempFile(@NonNull Context context) throws IOException {
        return File.createTempFile("tmp", ".data", context.getCacheDir());
    }

    @NonNull
    public static File tempFile(@NonNull Context context, @NonNull String filename) {
        return new File(context.getCacheDir(), filename);
    }

    @NonNull
    public static String getUniqueName(List<String> names, @NonNull String name) {
        return getName(names, name, 0);
    }

    @NonNull
    private static String getName(List<String> names, @NonNull String name, int index) {
        String searchName = name;
        if (index > 0) {
            try {
                String base = nameWithoutExtension(name);
                String extension = fileExtension(name);
                if (extension.isEmpty()) {
                    searchName = searchName.concat(" (" + index + ")");
                } else {
                    String end = " (" + index + ")";
                    if (base.endsWith(end)) {
                        String realBase = base.substring(0, base.length() - end.length());
                        searchName = realBase.concat(" (" + index + ")").concat(".").concat(extension);
                    } else {
                        searchName = base.concat(" (" + index + ")").concat(".").concat(extension);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                searchName = searchName.concat(" (" + index + ")"); // just backup
            }
        }

        if (names.contains(searchName)) {
            return getName(names, name, ++index);
        }
        return searchName;
    }

    public static String checkMimeType(@Nullable String mimeType, @NonNull String name) {
        boolean evalDisplayName = false;
        if (mimeType == null) {
            evalDisplayName = true;
        } else {
            if (mimeType.isEmpty()) {
                evalDisplayName = true;
            } else {
                if (Objects.equals(mimeType, MimeTypeService.OCTET_MIME_TYPE)) {
                    evalDisplayName = true;
                }
            }
        }
        if (evalDisplayName) {
            mimeType = MimeTypeService.getMimeType(name);
        }
        return mimeType;
    }


    public static boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.Network nw = connectivityManager.getActiveNetwork();
        if (nw == null) return false;
        NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
        return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET));
    }

    @NonNull
    private static Keys keys(@NonNull Context context) throws Exception {

        if (!getPrivateKey(context).isEmpty() && !getPublicKey(context).isEmpty()) {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] publicKey = decoder.decode(getPublicKey(context));
            byte[] privateKey = decoder.decode(getPrivateKey(context));
            return new Keys(PeerId.create(publicKey), privateKey);
        } else {
            Keys keys = Lite.generateKeys();
            Base64.Encoder encoder = Base64.getEncoder();
            setPrivateKey(context, encoder.encodeToString(keys.privateKey()));
            setPublicKey(context, encoder.encodeToString(keys.peerId().hash()));
            return keys;
        }
    }

    public static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));
    }

    public static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                IPFS_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    @NonNull
    public static Peeraddrs bootstrap() {
        // "/ip4/104.131.131.82/udp/4001/quic/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"
        Peeraddrs peeraddrs = new Peeraddrs();
        try {
            peeraddrs.add(Lite.createPeeraddr(
                    "QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ",
                    "104.131.131.82", 4001));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return peeraddrs;
    }

    @Nullable
    @TypeConverter
    public static Cid toCid(byte[] data) {
        if (data == null) {
            return null;
        }
        return Cid.toCid(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        if (cid == null) {
            return null;
        }
        return Cid.toArray(cid);
    }

    @NonNull
    @TypeConverter
    public static PeerId toPeerId(byte[] data) {
        return PeerId.toPeerId(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(PeerId peerId) {
        return PeerId.toArray(peerId);
    }

    public static void deleteCache(@NonNull Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private static boolean deleteDir(@Nullable File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public Session session() {
        return session;
    }

    private Envelope createEnvelope(Context context, Payload payload) {
        try {
            if (Objects.equals(PNS_RECORD, payload)) {
                Dir page = homePage(context);
                return Lite.createEnvelope(server, PNS_RECORD, page.cid());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    @NonNull
    public Uri homePageUri() {
        return Uri.parse(API.PNS + "://" + host);
    }


    public void initPage(@NonNull Context context) {
        lock.lock();
        try {
            String title = getTitle(context);
            List<FileInfo> fileInfos = files.getFileInfos();
            List<Info> fileLinks = new ArrayList<>();
            boolean isEmpty = fileInfos.isEmpty();
            if (!isEmpty) {
                for (FileInfo fileInfo : fileInfos) {
                    Cid link = fileInfo.cid();
                    Objects.requireNonNull(link);
                    fileLinks.add(new Fid(link, fileInfo.size(), fileInfo.name()));
                }
            }
            Dir dir = Lite.createDirectory(session, title, fileLinks);
            Objects.requireNonNull(dir);
            homepage.set(dir);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    public Dir homePage(@NonNull Context context) {
        Dir page = homepage.get();
        try {
            if (page == null) {
                String title = getTitle(context);
                page = Lite.createEmptyDirectory(session, title);
                Objects.requireNonNull(page);
                homepage.set(page);
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return page;
    }

    @NonNull
    public Server server() {
        return server;
    }

}
