package threads.odin.state;

import android.app.Application;
import android.content.ClipData;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.data.blocks.BLOCKS;
import threads.odin.data.conns.CONNS;
import threads.odin.data.conns.Conn;
import threads.odin.data.conns.Conns;
import threads.odin.data.files.FILES;
import threads.odin.data.files.FileInfo;
import threads.odin.data.files.Files;
import threads.odin.data.relays.RELAYS;
import threads.odin.data.relays.Relay;
import threads.odin.data.relays.Relays;
import threads.odin.model.API;
import threads.odin.work.UploadFileWorker;
import threads.odin.work.UploadFilesWorker;

public class StateModel extends AndroidViewModel {

    private static final String TAG = StateModel.class.getSimpleName();
    @NonNull
    private final MutableLiveData<Reachability> reachability = new MutableLiveData<>(Reachability.UNKNOWN);
    @NonNull
    private final MutableLiveData<String> error = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> title = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> warning = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<String> delete = new MutableLiveData<>(null);
    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);
    @NonNull
    private final MutableLiveData<Boolean> reservationActive = new MutableLiveData<>(false);
    @NonNull
    private final Files files;
    @NonNull
    private final Conns conns;
    @NonNull
    private final Relays relays;

    private final Lock hopReserve = new ReentrantLock();

    public StateModel(@NonNull Application application) {
        super(application);

        files = FILES.getInstance(application).database();
        conns = CONNS.getInstance(application).database();
        relays = RELAYS.getInstance(application).database();

        Timer timer = new Timer();

        long delay = 60 * 30 * 1000; // 30 min
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                hopReserve();
            }
        }, 5000, delay);

        title(API.getTitle(getApplication()));

    }

    public void hopReserve() {
        if (hopReserve.tryLock()) {
            long start = System.currentTimeMillis();
            try {
                API api = API.getInstance(getApplication());
                reservationActive(true);
                LogUtils.error(TAG, "HopReserve Start ...");
                Objects.requireNonNull(api.server());

                Lite.hopReserve(api.server(), 100, 180);

                for (Peeraddr peeraddr : Lite.reservationPeeraddrs(api.server())) {
                    LogUtils.error(TAG, "Dialable Address " + peeraddr.toString());
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.error(TAG, "HopReserve  Finish " +
                        " onStart [" + (System.currentTimeMillis() - start) + "]...");
                reservationActive(false);
                hopReserve.unlock();
            }
        }
    }

    @NonNull
    public LiveData<List<FileInfo>> liveDataFiles() {
        return files.getLiveDataFiles();
    }

    @NonNull
    public LiveData<List<Conn>> liveDataConns() {
        return conns.connDao().conns();
    }

    @NonNull
    public LiveData<List<Relay>> liveDataRelays() {
        return relays.relayDao().relays();
    }


    @NonNull
    public MutableLiveData<Boolean> reservationActive() {
        return reservationActive;
    }

    public void reservationActive(boolean active) {
        reservationActive.postValue(active);
    }

    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        showFab.postValue(show);
    }


    public MutableLiveData<String> title() {
        return title;
    }

    public void title(@Nullable String title) {
        title().postValue(title);
    }

    public MutableLiveData<String> error() {
        return error;
    }

    public void error(@Nullable String content) {
        error().postValue(content);
    }


    public MutableLiveData<String> delete() {
        return delete;
    }

    public void delete(@Nullable String content) {
        delete().postValue(content);
    }

    public MutableLiveData<String> warning() {
        return warning;
    }

    public void warning(@Nullable String content) {
        warning().postValue(content);
    }

    @NonNull
    public Uri homePageUri() throws Exception {
        return API.getInstance(getApplication()).homePageUri();
    }

    public void deleteFiles(long... idxs) {
        try {
            files.delete(idxs);
            API.getInstance(getApplication()).initPage(getApplication());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public String networkReachability(Reachability reachability) {

        return switch (reachability) {
            case UNKNOWN -> getApplication().getString(R.string.unknown);
            case LOCAL -> getApplication().getString(R.string.local_network);
            case RELAYS -> getApplication().getString(R.string.relays_network);
        };
    }

    @NonNull
    public Server server() throws Exception {
        return API.getInstance(getApplication()).server();
    }


    public void resetFilesDeleting(long... idxs) {
        try {
            for (long idx : idxs) {
                files.resetDeleting(idx);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void removeFiles(long... indices) {
        try {
            for (long idx : indices) {
                files.setDeleting(idx);
            }
            delete(Arrays.toString(indices));

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void evaluateReachability() {
        if (!Lite.reservationFeaturePossible()) {
            reachability(Reachability.LOCAL);
        } else {
            reachability(Reachability.RELAYS);
        }
    }

    public void reset() {
        long start = System.currentTimeMillis();
        try {
            LogUtils.info(TAG, " start ...");

            files.clear();
            BLOCKS.getInstance(getApplication()).clear();

            // delete cache
            API.deleteCache(getApplication());

            // Update data
            API.getInstance(getApplication()).initPage(getApplication());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
    }

    public void reachability(Reachability reachability) {
        reachability().postValue(reachability);
    }

    @NonNull
    public MutableLiveData<Reachability> reachability() {
        return reachability;
    }

    public void loadClipData(ClipData clipData) {

        try {
            Objects.requireNonNull(clipData);
            int items = clipData.getItemCount();

            if (items > 0) {
                File file = API.createTempFile(getApplication());
                try (PrintStream out = new PrintStream(file)) {
                    for (int i = 0; i < items; i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        Uri uri = item.getUri();
                        out.println(uri.toString());
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                UploadFilesWorker.load(getApplication(), file);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void loadUri(Uri uri) {

        try {
            List<String> names = files.fileNames();
            String displayName = API.fileName(getApplication(), uri);
            String uriType = API.mimeType(getApplication(), uri);

            String name = API.getUniqueName(names, displayName);
            String mimeType = API.checkMimeType(uriType, displayName);

            long size = API.fileSize(getApplication(), uri);

            FileInfo fileInfo = FILES.createFileInfo(name, mimeType, null, size);
            long idx = files.storeFileInfo(fileInfo);

            UploadFileWorker.load(getApplication(), uri, idx);

        } catch (Throwable throwable) {
            error(getApplication().getString(R.string.file_not_found));
        }
    }

    public enum Reachability {
        UNKNOWN, LOCAL, RELAYS
    }
}