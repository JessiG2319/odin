package threads.odin.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.data.files.FileInfo;
import threads.odin.model.API;

public class FilesViewAdapter extends RecyclerView.Adapter<FilesViewAdapter.ViewHolder> implements FileItemPosition {

    private static final String TAG = FilesViewAdapter.class.getSimpleName();
    private final Context mContext;

    private final List<FileInfo> fileInfos = new ArrayList<>();

    @Nullable
    private SelectionTracker<Long> mSelectionTracker;

    public FilesViewAdapter(@NonNull Context context) {
        this.mContext = context;
    }


    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.card_file;
    }

    @Override
    @NonNull
    public FilesViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(this, v);
    }

    long getIdx(int position) {
        return fileInfos.get(position).idx();
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final FileInfo fileInfo = this.fileInfos.get(position);


        boolean isSelected = false;
        if (mSelectionTracker != null) {
            if (mSelectionTracker.isSelected(fileInfo.idx())) {
                isSelected = true;
            }
        }

        holder.bind(isSelected, fileInfo);
        try {
            holder.card.setChecked(isSelected);

            int resId = MimeTypeService.getMediaResource(fileInfo.mimeType());
            holder.main_image.setImageResource(resId);

            String title = API.compactString(fileInfo.name());
            holder.name.setText(title);
            String info = API.size(fileInfo);
            holder.size.setText(info);

            UUID uuid = fileInfo.getWorkUUID();
            if (uuid != null) {
                holder.general_action.setVisibility(View.VISIBLE);
                holder.progress_bar.setVisibility(View.VISIBLE);

                holder.general_action.setEnabled(true);
                holder.general_action.setOnClickListener((v) ->
                        WorkManager.getInstance(mContext).cancelWorkById(uuid)
                );

            } else {
                holder.progress_bar.setVisibility(View.GONE);
                holder.general_action.setVisibility(View.GONE);
                holder.general_action.setEnabled(false);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }

    @Override
    public int getItemCount() {
        return fileInfos.size();
    }

    public void updateData(@NonNull List<FileInfo> fileInfos) {

        final FileDiffCallback diffCallback = new FileDiffCallback(this.fileInfos, fileInfos);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.fileInfos.clear();
        this.fileInfos.addAll(fileInfos);
        diffResult.dispatchUpdatesTo(this);
    }

    public void selectAll() {
        try {
            for (FileInfo fileInfo : fileInfos) {
                if (mSelectionTracker != null) {
                    mSelectionTracker.select(fileInfo.idx());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public int getPosition(long idx) {
        for (int i = 0; i < fileInfos.size(); i++) {
            if (fileInfos.get(i).idx() == idx) {
                return i;
            }
        }
        return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        final MaterialCardView card;
        final TextView name;
        final TextView size;
        final ImageView main_image;
        final MaterialButton general_action;
        final LinearProgressIndicator progress_bar;
        final FileItemDetails fileItemDetails;

        ViewHolder(FileItemPosition pos, View v) {
            super(v);
            card = v.findViewById(R.id.card);
            name = v.findViewById(R.id.name);
            size = v.findViewById(R.id.size);
            general_action = v.findViewById(R.id.general_action);
            progress_bar = v.findViewById(R.id.progress_bar);
            main_image = v.findViewById(R.id.main_image);
            fileItemDetails = new FileItemDetails(pos);
        }

        void bind(boolean isSelected, FileInfo fileInfo) {
            fileItemDetails.idx = fileInfo.idx();

            itemView.setActivated(isSelected);
        }

        @NonNull
        ItemDetailsLookup.ItemDetails<Long> getItemDetails() {
            return fileItemDetails;
        }
    }

}
