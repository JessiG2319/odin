package threads.odin.ui;


import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import java.util.Objects;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.model.API;
import threads.odin.state.StateModel;
import threads.odin.utils.FileItemDetailsLookup;
import threads.odin.utils.FilesItemKeyProvider;
import threads.odin.utils.FilesViewAdapter;
import threads.odin.utils.MimeTypeService;


public class FilesFragment extends Fragment {

    private static final String TAG = FilesFragment.class.getSimpleName();
    private long lastClickTime = 0;
    private StateModel stateModel;

    private final ActivityResultLauncher<Intent> mFilesImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                if (data.getClipData() != null) {
                                    ClipData clipData = data.getClipData();
                                    Objects.requireNonNull(clipData);
                                    // todo virtual thread
                                    new Thread(() -> stateModel.loadClipData(clipData)).start();
                                } else if (data.getData() != null) {
                                    Uri uri = data.getData();
                                    Objects.requireNonNull(uri);
                                    // todo virtual thread
                                    new Thread(() -> stateModel.loadUri(uri)).start();
                                }

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private FilesViewAdapter filesViewAdapter;
    private RecyclerView recyclerView;
    private ActionMode actionMode;
    private SelectionTracker<Long> selectionTracker;
    private ExtendedFloatingActionButton extendedFloatingActionButton;

    private static long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null) {
            selectionTracker.onSaveInstanceState(outState);
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_files, container, false);
    }


    private void clickFilesAdd() {

        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType(MimeTypeService.ALL);
            String[] mimeTypes = {MimeTypeService.ALL};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            mFilesImportForResult.launch(intent);

        } catch (Throwable throwable) {
            stateModel.warning(getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    private void showFab(boolean showFab) {
        if (showFab) {
            extendedFloatingActionButton.show();
        } else {
            extendedFloatingActionButton.hide();
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        BottomNavigationView bottomNavigationView = view.findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.refreshDrawableState();
        int badgeBackgroundColor = bottomNavigationView.getOrCreateBadge(R.id.navigation_relays)
                .getBackgroundColor();
        bottomNavigationView.setOnItemSelectedListener((item) -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();


            int itemId = item.getItemId();
            if (itemId == R.id.navigation_info) {
                showInformation();
                return true;
            } else if (itemId == R.id.navigation_relays) {
                RelaysFragment.newInstance().show(getChildFragmentManager(),
                        RelaysFragment.TAG);
                return true;
            } else if (itemId == R.id.navigation_connections) {
                ConnsFragment.newInstance().show(getChildFragmentManager(),
                        ConnsFragment.TAG);
                return true;
            } else {
                return false;
            }


        });

        extendedFloatingActionButton = view.findViewById(R.id.extended_floating_action_add_files);
        extendedFloatingActionButton.setOnClickListener((v) -> {
            if (SystemClock.elapsedRealtime() - lastClickTime < 500) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickFilesAdd();
        });


        stateModel = new ViewModelProvider(requireActivity()).get(StateModel.class);

        stateModel.liveDataRelays().observe(getViewLifecycleOwner(), (relays -> {
            try {
                if (relays != null) {
                    bottomNavigationView.getOrCreateBadge(
                            R.id.navigation_relays).setNumber(relays.size());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        stateModel.reservationActive().observe(getViewLifecycleOwner(), active -> {
            try {
                if (active != null) {
                    BadgeDrawable badge = bottomNavigationView.getOrCreateBadge(
                            R.id.navigation_relays);
                    if (active) {
                        badge.setBackgroundColor(
                                getResources().getColor(R.color.ic_launcher_background,
                                        requireActivity().getTheme()));
                    } else {
                        badge.setBackgroundColor(badgeBackgroundColor);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        stateModel.liveDataConns().observe(getViewLifecycleOwner(), (conns -> {
            try {
                if (conns != null) {
                    bottomNavigationView.getOrCreateBadge(
                            R.id.navigation_connections).setNumber(conns.size());
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));

        stateModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        TextView reachability = view.findViewById(R.id.reachability);
        Objects.requireNonNull(reachability);
        stateModel.reachability().observe(getViewLifecycleOwner(), value -> {
            try {
                if (value != null) {
                    reachability.setText(stateModel.networkReachability(value));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swipe_container);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                swipeRefreshLayout.setRefreshing(true);
                // todo virtual thread
                new Thread(() -> stateModel.hopReserve()).start();

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        recyclerView = view.findViewById(R.id.recycler_files);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(linearLayoutManager);


        filesViewAdapter = new FilesViewAdapter(requireContext());
        recyclerView.setAdapter(filesViewAdapter);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    boolean hasSelection = selectionTracker.hasSelection();
                    if (dy > 0) {
                        stateModel.setShowFab(false);
                    } else if (dy < 0 && !hasSelection) {
                        stateModel.setShowFab(true);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });


        selectionTracker = new SelectionTracker.Builder<>(TAG, recyclerView,
                new FilesItemKeyProvider(filesViewAdapter),
                new FileItemDetailsLookup(recyclerView),
                StorageStrategy.createLongStorage())
                .build();


        selectionTracker.addObserver(new SelectionTracker.SelectionObserver<>() {
            @Override
            public void onSelectionChanged() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle(String.valueOf(selectionTracker.getSelection().size()));
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle(String.valueOf(selectionTracker.getSelection().size()));
                }
                super.onSelectionRestored();
            }
        });

        filesViewAdapter.setSelectionTracker(selectionTracker);


        stateModel.liveDataFiles().observe(getViewLifecycleOwner(), (fileInfos) -> {

            try {
                if (fileInfos != null) {
                    int size = filesViewAdapter.getItemCount();
                    boolean scrollToTop = size < fileInfos.size();

                    filesViewAdapter.updateData(fileInfos);

                    if (scrollToTop) {
                        try {
                            recyclerView.scrollToPosition(0);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        if (savedInstanceState != null) {
            selectionTracker.onRestoreInstanceState(savedInstanceState);
        }

    }

    private void showInformation() {
        try {
            Uri uri = stateModel.homePageUri();
            InfoFragment.newInstance(uri)
                    .show(getChildFragmentManager(), InfoFragment.TAG);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void deleteAction() {

        if (!selectionTracker.hasSelection()) {
            stateModel.warning(getString(R.string.no_marked_file_delete));
            return;
        }

        try {
            long[] entries = convert(selectionTracker.getSelection());

            selectionTracker.clearSelection();

            // todo virtual thread
            new Thread(() -> stateModel.removeFiles(entries)).start();


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_files_action_mode, menu);

                stateModel.setShowFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_mark_all) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    filesViewAdapter.selectAll();

                    return true;
                } else if (itemId == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                try {
                    selectionTracker.clearSelection();

                    stateModel.setShowFab(true);

                    if (actionMode != null) {
                        actionMode = null;
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        };

    }

    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (actionMode != null) {
                    actionMode.finish();
                    actionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
