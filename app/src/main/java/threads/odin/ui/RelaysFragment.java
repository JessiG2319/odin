package threads.odin.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.state.StateModel;
import threads.odin.utils.RelaysAdapter;

public class RelaysFragment extends BottomSheetDialogFragment {

    public static final String TAG = RelaysFragment.class.getSimpleName();


    public static RelaysFragment newInstance() {
        return new RelaysFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.fragment_relays);

        TextView textViewTitle = dialog.findViewById(R.id.title);
        Objects.requireNonNull(textViewTitle);
        textViewTitle.setText(R.string.relays);


        RecyclerView recyclerView = dialog.findViewById(R.id.relays);
        Objects.requireNonNull(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        StateModel stateModel =
                new ViewModelProvider(requireActivity()).get(StateModel.class);

        RelaysAdapter relaysAdapter = new RelaysAdapter();
        recyclerView.setAdapter(relaysAdapter);


        stateModel.liveDataRelays().observe(this, (relays -> {
            try {
                if (relays != null) {
                    relaysAdapter.updateData(relays);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));
        return dialog;
    }
}
