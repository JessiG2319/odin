package threads.odin.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.state.StateModel;
import threads.odin.utils.ConnsAdapter;

public class ConnsFragment extends BottomSheetDialogFragment {

    public static final String TAG = ConnsFragment.class.getSimpleName();


    public static ConnsFragment newInstance() {
        return new ConnsFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.fragment_conns);

        TextView textViewTitle = dialog.findViewById(R.id.title);
        Objects.requireNonNull(textViewTitle);
        textViewTitle.setText(R.string.connections);


        RecyclerView recyclerView = dialog.findViewById(R.id.conns);
        Objects.requireNonNull(recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        StateModel stateModel =
                new ViewModelProvider(requireActivity()).get(StateModel.class);

        ConnsAdapter connsAdapter = new ConnsAdapter();
        recyclerView.setAdapter(connsAdapter);


        stateModel.liveDataConns().observe(this, (conns -> {
            try {
                if (conns != null) {
                    connsAdapter.updateData(conns);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }));
        return dialog;
    }
}
