package threads.odin.ui;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import tech.lp2p.core.Server;
import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.model.API;
import threads.odin.state.StateModel;
import threads.odin.utils.MimeTypeService;

public class InfoFragment extends BottomSheetDialogFragment {

    public static final String TAG = InfoFragment.class.getSimpleName();


    public static InfoFragment newInstance(@NonNull Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putString(API.URL, uri.toString());
        InfoFragment fragment = new InfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        BottomSheetBehavior<FrameLayout> behavior = dialog.getBehavior();
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setPeekHeight(0);

        dialog.setContentView(R.layout.fragment_info);


        StateModel stateModel = new ViewModelProvider(requireActivity()).get(StateModel.class);
        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);
        String title = getString(R.string.information);
        String url = bundle.getString(API.URL, "");

        TextView textViewTitle = dialog.findViewById(R.id.title);
        Objects.requireNonNull(textViewTitle);
        textViewTitle.setText(title);

        ImageView imageView = dialog.findViewById(R.id.uri_qrcode);
        Objects.requireNonNull(imageView);
        TextView page = dialog.findViewById(R.id.page);
        Objects.requireNonNull(page);

        if (url.isEmpty()) {
            page.setVisibility(View.GONE);
        } else {
            page.setText(url);
        }

        TextView important = dialog.findViewById(R.id.important);
        Objects.requireNonNull(important);


        TextView textImportant = dialog.findViewById(R.id.text_important);
        Objects.requireNonNull(textImportant);


        try {
            Server server = stateModel.server();
            Objects.requireNonNull(server);
            long port = server.port();
            Bitmap bitmapRight = MimeTypeService.getPortBitmap(requireContext(), port);


            Bitmap bitmapLeft = MimeTypeService.getIPvBitmap(requireContext());

            Drawable drawableLeft = new BitmapDrawable(getResources(), bitmapLeft);
            Drawable drawableRight = new BitmapDrawable(getResources(), bitmapRight);
            important.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    drawableLeft, null, drawableRight, null);


            textImportant.setText(getString(R.string.limitation));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        Bitmap bitmap = MimeTypeService.getBitmap(url);
        imageView.setImageBitmap(bitmap);

        return dialog;
    }
}
