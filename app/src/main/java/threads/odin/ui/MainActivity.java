package threads.odin.ui;


import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.search.SearchBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.odin.LogUtils;
import threads.odin.R;
import threads.odin.model.API;
import threads.odin.state.StateModel;
import threads.odin.utils.MimeTypeService;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private long lastClickTime = 0;
    private LinearLayout mainLayout;
    private StateModel stateModel;


    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        try {
            invalidateOptionsMenu();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }

    public void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);


            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(@NonNull Network network) {
                    try {
                        stateModel.evaluateReachability();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                public void onCapabilitiesChanged(@NonNull Network network,
                                                  @NonNull NetworkCapabilities networkCapabilities) {
                    try {
                        stateModel.evaluateReachability();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                @Override
                public void onLost(@NonNull Network network) {
                    try {
                        stateModel.reachability(StateModel.Reachability.UNKNOWN);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                showHomeDialog();

                return true;
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        } else if (item.getItemId() == R.id.action_share) {

            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                String link = stateModel.homePageUri().toString();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                intent.putExtra(Intent.EXTRA_TEXT, link);
                intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooser);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        } else if (item.getItemId() == R.id.action_documentation) {

            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://gitlab.com/lp2p/odin"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } catch (Throwable throwable) {
                stateModel.warning(getString(R.string.no_activity_found_to_handle_uri));
            }

            return true;
        } else if (item.getItemId() == R.id.action_reset) {

            if (SystemClock.elapsedRealtime() - lastClickTime < API.CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(MainActivity.this);
                alertDialog.setTitle(getString(R.string.warning));
                alertDialog.setMessage(getString(R.string.reset_application_data));
                alertDialog.setPositiveButton(getString(android.R.string.ok),
                        (dialogInterface, which) -> {
                            // todo virtual thread
                            new Thread(() -> stateModel.reset()).start();
                            dialogInterface.dismiss();
                        });
                alertDialog.setNeutralButton(getString(android.R.string.cancel),
                        (dialogInterface, which) -> dialogInterface.dismiss());
                alertDialog.show();

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SearchBar searchBar = findViewById(R.id.search_bar);
        setSupportActionBar(searchBar);

        searchBar.setOnClickListener(v -> {
            try {
                Uri uri = stateModel.homePageUri();

                InfoFragment.newInstance(uri)
                        .show(getSupportFragmentManager(), InfoFragment.TAG);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mainLayout = findViewById(R.id.main_layout);


        stateModel = new ViewModelProvider(this).get(StateModel.class);


        stateModel.title().observe(this, (title) -> {
            try {
                if (title != null) {
                    searchBar.setText(title);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        stateModel.delete().observe(this, (content) -> {
            try {
                if (content != null) {

                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_file);
                        } else {

                            message = String.valueOf(idxs.length)
                                    .concat(" ")
                                    .concat(getString(R.string.delete_files));
                        }
                        AtomicBoolean deleteThreads = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG);

                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                deleteThreads.set(false);
                                // todo virtual thread
                                new Thread(() -> stateModel.resetFilesDeleting(idxs)).start();
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                snackbar.dismiss();
                            }

                        });

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (deleteThreads.get()) {
                                    // todo virtual thread
                                    new Thread(() -> stateModel.deleteFiles(idxs)).start();
                                }
                                stateModel.setShowFab(true);

                            }
                        });
                        stateModel.setShowFab(false);
                        snackbar.setAnchorView(R.id.bottom_navigation_view);
                        snackbar.show();
                    }

                    stateModel.delete(null);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.error().observe(this, (content) -> {
            try {
                if (content != null) {

                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                stateModel.setShowFab(true);

                            }
                        });
                        stateModel.setShowFab(false);
                        snackbar.setAnchorView(R.id.bottom_navigation_view);
                        snackbar.show();
                    }
                    stateModel.error(null);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        stateModel.warning().observe(this, (content) -> {
            try {
                if (content != null) {

                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mainLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                stateModel.setShowFab(true);

                            }
                        });
                        stateModel.setShowFab(false);
                        snackbar.setAnchorView(R.id.bottom_navigation_view);
                        snackbar.show();
                    }
                    stateModel.warning(null);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
    }

    private void showHomeDialog() {
        try {
            final View view = ((LayoutInflater)
                    getSystemService(LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.fragment_home, null);

            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(
                    this);

            TextInputLayout textInputLayout = view.findViewById(R.id.title_input_layout);
            textInputLayout.setCounterEnabled(true);
            textInputLayout.setCounterMaxLength(50);


            TextInputEditText textInputEditText = view.findViewById(R.id.title_text);
            InputFilter[] filterTitle = new InputFilter[1];
            filterTitle[0] = new InputFilter.LengthFilter(50);
            textInputEditText.setFilters(filterTitle);
            textInputEditText.setText(API.getTitle(getApplicationContext()));
            textInputEditText.requestFocus();


            builder.setView(view).setTitle(R.string.home);

            AlertDialog dialog = builder.create();

            Window window = dialog.getWindow();
            Objects.requireNonNull(window);
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.TOP;

            window.setAttributes(lp);

            textInputEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String name = s.toString();

                    if (name.isEmpty()) {
                        textInputLayout.setError(getString(R.string.name_not_valid));
                    } else {
                        textInputLayout.setError(null);
                    }
                }
            });

            textInputEditText.setOnEditorActionListener((v, actionId, event) -> {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    try {
                        Editable text = textInputEditText.getText();
                        Objects.requireNonNull(text);

                        String name = text.toString();

                        if (name.isEmpty()) {
                            textInputLayout.setError(getString(R.string.name_not_valid));
                            return false;
                        } else {
                            textInputLayout.setError(null);
                        }

                        InputMethodManager imm = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(textInputEditText.getWindowToken(), 0);

                        if (!Objects.equals(name, API.getTitle(getApplicationContext()))) {
                            API.setTitle(getApplicationContext(), name);
                            stateModel.title(name);
                        }

                        dialog.cancel();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                    return true;
                }
                return handled;
            });

            textInputEditText.requestFocus();


            dialog.show();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}