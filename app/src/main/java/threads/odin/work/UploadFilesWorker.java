package threads.odin.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Session;
import threads.odin.LogUtils;
import threads.odin.data.files.FILES;
import threads.odin.data.files.FileInfo;
import threads.odin.model.API;

public final class UploadFilesWorker extends Worker {
    private static final String TAG = UploadFilesWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFilesWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    public static OneTimeWorkRequest getWork(@NonNull File file) {

        Data.Builder data = new Data.Builder();
        data.putString(API.FILE, file.getName());

        return new OneTimeWorkRequest.Builder(UploadFilesWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, @NonNull File file) {
        WorkManager.getInstance(context).enqueue(getWork(file));
    }


    @NonNull
    @Override
    public Result doWork() {

        String filename = getInputData().getString(API.FILE);

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ... ");

        try {
            FILES files = FILES.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());

            Objects.requireNonNull(filename);
            File file = API.tempFile(getApplicationContext(), filename);
            Objects.requireNonNull(file);

            List<String> uris = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(file)))) {
                Objects.requireNonNull(reader);
                while (reader.ready()) {
                    uris.add(reader.readLine());
                }
            }

            try {
                List<String> names = files.fileNames();
                for (String uriStr : uris) {
                    Uri uri = Uri.parse(uriStr);

                    String displayName = API.fileName(getApplicationContext(), uri);
                    String uriType = API.mimeType(getApplicationContext(), uri);

                    String name = API.getUniqueName(names, displayName);
                    String mimeType = API.checkMimeType(uriType, displayName);

                    long size = API.fileSize(getApplicationContext(), uri);

                    FileInfo fileInfo = FILES.createFileInfo(name, mimeType, null, size);
                    long idx = files.storeFileInfo(fileInfo);

                    files.setWork(idx, getId());

                    try (InputStream inputStream = getApplicationContext().getContentResolver()
                            .openInputStream(uri)) {
                        Objects.requireNonNull(inputStream);
                        Session session = api.session();
                        Objects.requireNonNull(session);

                        Fid fid = Lite.storeInputStream(session, name, inputStream, progress -> {
                            if (isStopped()) {
                                throw new IllegalStateException("Progress is stopped");
                            }
                        }, size);

                        Objects.requireNonNull(fid);
                        files.setDone(idx, fid.cid());
                    } catch (Throwable throwable) {
                        files.delete(idx);
                        throw throwable;
                    } finally {
                        names.add(name); // just for safety
                    }
                }
            } finally {
                api.initPage(getApplicationContext());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }


}
