package threads.odin.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;

import tech.lp2p.Lite;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Session;
import threads.odin.LogUtils;
import threads.odin.data.files.FILES;
import threads.odin.data.files.FileInfo;
import threads.odin.model.API;

public final class UploadFileWorker extends Worker {
    private static final String TAG = UploadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putString(API.URI, uri.toString());
        data.putLong(API.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueue(getWork(uri, idx));
    }


    @NonNull
    @Override
    public Result doWork() {


        long idx = getInputData().getLong(API.IDX, -1);
        String url = getInputData().getString(API.URI);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);


        try {
            API api = API.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());

            files.setWork(idx, getId());

            FileInfo fileInfo = files.getFileInfo(idx);
            Objects.requireNonNull(fileInfo);

            Uri uri = Uri.parse(url);

            long size = fileInfo.size();


            try (InputStream inputStream = getApplicationContext().getContentResolver()
                    .openInputStream(uri)) {
                Objects.requireNonNull(inputStream);
                Session session = api.session();
                Objects.requireNonNull(session);
                Fid fid = Lite.storeInputStream(session, fileInfo.name(), inputStream, progress -> {
                    if (isStopped()) {
                        throw new IllegalStateException("Progress is stopped");
                    }
                }, size);

                Objects.requireNonNull(fid);
                files.setDone(idx, fid.cid());
            } catch (Throwable throwable) {
                files.delete(idx);
                throw throwable;
            } finally {
                api.initPage(getApplicationContext());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

}
