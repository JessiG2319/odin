package threads.odin.data.conns;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;

@Entity
public record Conn(@PrimaryKey @ColumnInfo(name = "peerId") @NonNull PeerId peerId,
                   @ColumnInfo(typeAffinity = ColumnInfo.BLOB) byte[] raw) {
    @NonNull
    public static Conn create(Peeraddr peeraddr) {
        return new Conn(peeraddr.peerId(), peeraddr.encoded());
    }

    @NonNull
    public Peeraddr peeraddr() {
        return Objects.requireNonNull(Peeraddr.create(peerId, raw));
    }


    @Override
    @NonNull
    public PeerId peerId() {
        return peerId;
    }

    @Override
    public byte[] raw() {
        return raw;
    }

}
