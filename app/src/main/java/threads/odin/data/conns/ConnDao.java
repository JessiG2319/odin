package threads.odin.data.conns;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.PeerId;

@Dao
public interface ConnDao {

    @Query("SELECT * FROM Conn")
    LiveData<List<Conn>> conns();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertConn(Conn conn);

    @Query("DELETE FROM Conn WHERE peerId = :peerId")
    void deleteConn(PeerId peerId);

}
