package threads.odin.data.conns;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.odin.model.API;

@Database(entities = {Conn.class}, version = 2, exportSchema = false)
@TypeConverters({API.class})
public abstract class Conns extends RoomDatabase {

    public abstract ConnDao connDao();

}
