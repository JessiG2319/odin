package threads.odin.data.conns;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import tech.lp2p.core.Peeraddr;

public class CONNS {


    private static volatile CONNS INSTANCE = null;
    private final Conns conns;

    private CONNS(Conns database) {
        this.conns = database;
    }


    public static CONNS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (CONNS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new CONNS(Room.inMemoryDatabaseBuilder(context, Conns.class).build());
                }
            }
        }
        return INSTANCE;
    }


    public void addConn(@NonNull Peeraddr peeraddr) {
        conns.connDao().insertConn(Conn.create(peeraddr));
    }


    public void removeConn(@NonNull Peeraddr peeraddr) {
        conns.connDao().deleteConn(peeraddr.peerId());
    }

    @NonNull
    public Conns database() {
        return conns;
    }
}
