package threads.odin.data.relays;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import tech.lp2p.core.Reservation;

public class RELAYS {


    private static volatile RELAYS INSTANCE = null;
    private final Relays relays;

    private RELAYS(Relays database) {
        this.relays = database;
    }


    public static RELAYS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (RELAYS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new RELAYS(Room.inMemoryDatabaseBuilder(context,
                            Relays.class).build());
                }
            }
        }
        return INSTANCE;
    }


    public void addRelay(@NonNull Reservation reservation) {
        relays.insert(reservation.peeraddr());
    }


    public void removeRelay(@NonNull Reservation reservation) {
        relays.remove(reservation.peeraddr());
    }

    @NonNull
    public Relays database() {
        return relays;
    }
}
