package threads.odin.data.files;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import tech.lp2p.core.Cid;

@Dao
public interface FilesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFileInfo(FileInfo fileInfo);

    @Query("UPDATE FileInfo SET deleting = 1 WHERE idx = :idx")
    void setDeleting(long idx);

    @Query("UPDATE FileInfo SET deleting = 0 WHERE idx = :idx")
    void resetDeleting(long idx);

    @Query("SELECT * FROM FileInfo WHERE idx =:idx")
    FileInfo getFileInfo(long idx);

    @Query("SELECT * FROM FileInfo WHERE deleting = 0")
    LiveData<List<FileInfo>> getLiveDataFiles();

    @Query("SELECT * FROM FileInfo WHERE deleting = 0 AND work IS NULL")
    List<FileInfo> getFileInfos();

    @Query("UPDATE FileInfo SET cid =:cid, work = NULL WHERE idx = :idx")
    void setDone(long idx, Cid cid);

    @Query("UPDATE FileInfo SET work = :work WHERE idx = :idx")
    void setWork(long idx, String work);

    @Query("DELETE FROM FileInfo WHERE idx = :idx")
    void delete(long idx);

    @Query("SELECT name FROM FileInfo WHERE deleting = 0")
    List<String> getNames();
}
