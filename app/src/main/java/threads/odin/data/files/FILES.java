package threads.odin.data.files;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;
import java.util.UUID;

import tech.lp2p.core.Cid;

public class FILES {

    private static volatile FILES INSTANCE = null;

    private final Files files;


    private FILES(Files database) {
        this.files = database;
    }

    public static FILES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (FILES.class) {
                if (INSTANCE == null) {
                    INSTANCE = new FILES(Room.databaseBuilder(context,
                                    Files.class, Files.class.getSimpleName()).
                            fallbackToDestructiveMigration().build());
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static FileInfo createFileInfo(@NonNull String name, @NonNull String mimeType,
                                          @Nullable Cid cid, long size) {
        return new FileInfo(0, name, mimeType, cid, size, null, false);
    }


    @NonNull
    public Files database() {
        return files;
    }

    public void setDone(long idx, @NonNull Cid cid) {
        files.filesDao().setDone(idx, cid);
    }


    public long storeFileInfo(@NonNull FileInfo fileInfo) {
        return files.storeFileInfo(fileInfo);
    }

    @NonNull
    public List<FileInfo> getFileInfos() {
        return files.filesDao().getFileInfos();
    }

    @NonNull
    public List<String> fileNames() {
        return files.fileNames();
    }

    @Nullable
    public FileInfo getFileInfo(long idx) {
        return files.filesDao().getFileInfo(idx);
    }

    public void setWork(long idx, @NonNull UUID id) {
        files.filesDao().setWork(idx, id.toString());
    }

    public void delete(long idx) {
        files.delete(idx);
    }
}
