package threads.odin.data.files;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;
import java.util.UUID;

import tech.lp2p.core.Cid;


@Entity
public record FileInfo(@PrimaryKey(autoGenerate = true) long idx,
                       @NonNull @ColumnInfo(name = "name") String name,
                       @NonNull @ColumnInfo(name = "mimeType") String mimeType,
                       @Nullable @ColumnInfo(name = "cid") Cid cid,
                       @ColumnInfo(name = "size") long size,
                       @Nullable @ColumnInfo(name = "work") String work,
                       @ColumnInfo(name = "deleting") boolean deleting) {


    @Override
    @Nullable
    public Cid cid() {
        return cid;
    }

    @Override
    public long idx() {
        return idx;
    }

    @Override
    @NonNull
    public String mimeType() {
        return mimeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return idx() == fileInfo.idx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx());
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    @NonNull
    public String name() {
        return name;
    }

    @Override
    public boolean deleting() {
        return deleting;
    }

    @Override
    @Nullable
    public String work() {
        return work;
    }

    @Nullable
    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }


}
