package threads.odin.data.files;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.util.List;

import threads.odin.model.API;


@Database(entities = {FileInfo.class}, version = 1, exportSchema = false)
@TypeConverters({API.class})
public abstract class Files extends RoomDatabase {

    public abstract FilesDao filesDao();


    @NonNull
    public List<String> fileNames() {
        return filesDao().getNames();
    }

    public long storeFileInfo(@NonNull FileInfo fileInfo) {
        return filesDao().insertFileInfo(fileInfo);
    }

    private void delete(long idx) {
        filesDao().delete(idx);
    }

    public void delete(long... idxs) {
        for (long idx : idxs) {
            delete(idx);
        }
    }

    public LiveData<List<FileInfo>> getLiveDataFiles() {
        return filesDao().getLiveDataFiles();
    }

    public void resetDeleting(long idx) {
        filesDao().resetDeleting(idx);
    }

    public void clear() {
        clearAllTables();
    }

    public void setDeleting(long idx) {
        filesDao().setDeleting(idx);
    }
}
