package threads.odin.data.peers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;


public final class PEERS implements PeerStore {

    private static volatile PEERS INSTANCE = null;
    private final Peers peers;

    private PEERS(Peers database) {
        this.peers = database;
    }

    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new PEERS(Room.databaseBuilder(context,
                                    Peers.class,
                                    Peers.class.getSimpleName()).
                            fallbackToDestructiveMigration().build());
                }
            }
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public List<Peeraddr> peeraddrs(int limit) {
        List<Peeraddr> peeraddrs = new ArrayList<>();
        List<Peer> list = peers.bootstrapDao().randomPeers(limit);
        for (Peer peer : list) {
            peeraddrs.add(peer.peeraddr());
        }
        return peeraddrs;
    }

    @Override
    public void storePeeraddr(Peeraddr peeraddr) {
        peers.bootstrapDao().insertPeer(Peer.create(peeraddr));
    }


    @Override
    public void removePeeraddr(Peeraddr peeraddr) {
        peers.bootstrapDao().removePeer(peeraddr.peerId());
    }

}
