package threads.odin.data.blocks;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Block.class}, version = 1, exportSchema = false)
@TypeConverters({Block.class})
public abstract class Blocks extends RoomDatabase {

    public abstract BlocksDao blockDao();

}
