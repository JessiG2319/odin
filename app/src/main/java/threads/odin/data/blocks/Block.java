package threads.odin.data.blocks;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

import tech.lp2p.core.Cid;

@Entity
public record Block(
        @PrimaryKey @NonNull @ColumnInfo(name = "cid") Cid cid,
        @NonNull @ColumnInfo(name = "data", typeAffinity = ColumnInfo.BLOB) byte[] data) {


    /**
     * @noinspection unused
     */
    @NonNull
    @TypeConverter
    public static Cid toCid(byte[] data) {
        return Cid.toCid(data);
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        return Cid.toArray(cid);
    }

    @Override
    @NonNull
    public Cid cid() {
        return cid;
    }

    @Override
    @NonNull
    public byte[] data() {
        return data;
    }
}
