package threads.odin;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import threads.odin.data.blocks.BLOCKS;
import threads.odin.data.peers.PEERS;
import threads.odin.model.API;


final class TestEnv {

    public static final String AGENT = "lite/1.0.0/";
    private static final String TAG = TestEnv.class.getSimpleName();
    @NonNull
    private static final AtomicReference<Server> SERVER = new AtomicReference<>();
    @NonNull
    private static final ReentrantLock reserve = new ReentrantLock();
    private static Lite.Settings SETTINGS;


    private static volatile Lite INSTANCE = null;

    static {
        try {
            SETTINGS = Lite.createSettings(Lite.generateKeys(), API.bootstrap(), AGENT);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @NonNull
    public static Lite getInstance(@NonNull Lite.Settings settings) throws Exception {
        if (INSTANCE == null) {
            synchronized (Lite.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Lite(settings);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    static File createCacheFile(Context context) throws IOException {
        return File.createTempFile("temp", ".cid", context.getCacheDir());
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }


    public static Fid createContent(Session session, String name, byte[] data) throws Exception {
        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return Lite.storeInputStream(session, name, inputStream);
        }
    }

    @Nullable
    public static Server getServer() {
        return SERVER.get();
    }

    public static Lite getTestInstance(@NonNull Context context) throws Exception {
        reserve.lock();
        try {
            Objects.requireNonNull(SETTINGS);
            Lite lite = TestEnv.getInstance(SETTINGS);

            if (SERVER.get() == null) {

                Server server = lite.startServer(
                        Lite.createServerSettings(5001),
                        BLOCKS.getInstance(context),
                        PEERS.getInstance(context),
                        peeraddr -> LogUtils.error(TAG, "Incoming connection : "
                                + peeraddr.toString()),
                        peeraddr -> LogUtils.error(TAG, "Closing connection : "
                                + peeraddr.toString()),
                        reservationGain -> LogUtils.error(TAG, "Reservation gain " +
                                reservationGain.toString()),
                        reservationLost -> LogUtils.error(TAG, "Reservation lost " +
                                reservationLost.toString()),
                        peerId -> {
                            LogUtils.info(TAG, "Peer Gated : " + peerId.toString());
                            return false;
                        },
                        request -> null,
                        envelope -> {
                        });

                // just to get coverage of shutdown method
                Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));
                SERVER.set(server);

                if (API.isNetworkConnected(context)) {
                    Lite.hopReserve(server, 25, 120);

                    Peeraddrs set = Lite.reservationPeeraddrs(server);
                    for (Peeraddr peeraddr : set) {
                        LogUtils.info(TAG, "Dialable Address " + peeraddr.toString());
                    }
                }
            }

            return lite;
        } finally {
            reserve.unlock();
        }
    }

    public static class DummyBlockStore implements BlockStore {
        @Override
        public boolean hasBlock(@NonNull Cid cid) {
            return false;
        }

        @Nullable
        @Override
        public byte[] getBlock(@NonNull Cid cid) {
            return null;
        }


        @Override
        public void deleteBlock(@NonNull Cid cid) {

        }

        @Override
        public void storeBlock(@NonNull Cid cid, @NonNull byte[] block) {

        }

    }

    public static class DummyPeerStore implements PeerStore {
        @Override
        public List<Peeraddr> peeraddrs(int limit) {
            return Collections.emptyList();
        }

        @Override
        public void storePeeraddr(Peeraddr peeraddr) {

        }

        @Override
        public void removePeeraddr(Peeraddr peeraddr) {

        }
    }
}
