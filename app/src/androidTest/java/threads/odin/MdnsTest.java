package threads.odin;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import threads.odin.data.blocks.BLOCKS;
import threads.odin.data.peers.PEERS;
import threads.odin.model.MDNS;


public class MdnsTest {

    private static final String TAG = MdnsTest.class.getSimpleName();
    private static final String MDNS_SERVICE = "_test._udp.";
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    // This test only works when another mdns is online (probably kubo installation, daemon running in LAN)
    @Test
    public void mdns_test() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        MDNS serverMdns = MDNS.mdns(context, MDNS_SERVICE);
        serverMdns.startService(server);

        try {
            CountDownLatch finishFirst = new CountDownLatch(1);

            long start = System.currentTimeMillis();
            MDNS mdns = MDNS.mdns(context, MDNS_SERVICE);

            mdns.startDiscovery(peer -> {
                LogUtils.info(TAG, peer.toString());
                finishFirst.countDown();
            }); // not invoked in the test

            boolean finished = finishFirst.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");

            assertTrue(finished);
            mdns.stop();

            Thread.sleep(3000); // 3 sec to recover

            CountDownLatch finishSecond = new CountDownLatch(1);

            start = System.currentTimeMillis();
            mdns = MDNS.mdns(context, MDNS_SERVICE);

            mdns.startDiscovery(multiaddr -> {
                LogUtils.info(TAG, multiaddr.toString());
                finishSecond.countDown();
            }); // not invoked in the test


            finished = finishSecond.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");


            assertTrue(finished);
            mdns.stop();
        } finally {
            serverMdns.stop();
        }

    }

    @Test
    public void mdns_test_access() throws Exception {
        Lite lite = TestEnv.getTestInstance(context);
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        MDNS serverMdns = MDNS.mdns(context, MDNS_SERVICE);
        serverMdns.startService(server);


        Dummy dummy = new Dummy();

        try {

            CountDownLatch finishFirst = new CountDownLatch(1);

            long start = System.currentTimeMillis();
            final AtomicReference<Peeraddr> reference = new AtomicReference<>(null);
            MDNS mdns = MDNS.mdns(context, MDNS_SERVICE);

            mdns.startDiscovery(peeraddr -> {
                LogUtils.info(TAG, peeraddr.toString());
                reference.set(peeraddr);
                finishFirst.countDown();
            });

            boolean finished = finishFirst.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");

            assertTrue(finished);
            mdns.stop();

            Peeraddr peeraddr = reference.get();
            assertNotNull(peeraddr);

            Session session = lite.createSession(
                    BLOCKS.getInstance(context), PEERS.getInstance(context));

            byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

            Fid fid = TestEnv.createContent(session, "random.bin", input);
            TestCase.assertNotNull(fid);

            Session dummySession = dummy.createSession();
            Parameters parameters = Parameters.create(ALPN.lite, false);
            Connection connection = Lite.dial(dummySession, peeraddr, parameters);
            Objects.requireNonNull(connection);
            TestCase.assertTrue(connection.isConnected());

            start = System.currentTimeMillis();
            File file = TestEnv.createCacheFile(context);

            Lite.fetchToFile(session, file, fid.cid());


            assertEquals(file.length(), input.length);

            long end = System.currentTimeMillis();
            LogUtils.info(TAG, "Time for downloading " + (end - start) / 1000 +
                    "[s]" + " " + file.length() / 1000 + " [KB]");
            file.deleteOnExit();


            connection.close();



        } finally {
            serverMdns.stop();
        }
    }
}
