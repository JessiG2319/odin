package threads.odin;

import androidx.annotation.NonNull;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Session;
import tech.lp2p.lite.LiteHost;
import tech.lp2p.lite.LiteSession;
import threads.odin.model.API;

public final class Dummy {

    @NonNull
    private final LiteHost host;
    @NonNull
    private final BlockStore blockStore = new TestEnv.DummyBlockStore();
    private final PeerStore peerStore = new TestEnv.DummyPeerStore();

    public Dummy() throws Exception {
        this.host = new LiteHost(Lite.generateKeys(), API.bootstrap(), TestEnv.AGENT);
    }

    /**
     * @noinspection unused
     */
    @NonNull
    public PeerId self() {
        return host.self();
    }

    @NonNull
    public Session createSession() {
        return new LiteSession(blockStore, peerStore, host);
    }

}
