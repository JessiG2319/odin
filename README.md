# Odin

**Odin** is an decentralized peer-to-peer application to publish content via the **libp2p**
network stack. For sharing data, a **pns://** Uri is provided, which consists basically of a peer
id, which enables others to find your running server on the network.

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**Odin** is a server only implementation. The related client application can be found at the
following location [Thor](https://gitlab.com/lp2p/thor).

The reason for the strict separation between client and server, relies in performance reasons.

The client **Thor** is available in several app stores (like F-Droid, Google Play).

**Odin** based on a subset
of [IPFS](https://ipfs.io/), [IPNS](https://docs.ipfs.tech/concepts/ipns/#mutability-in-ipfs)
and [libp2p](https://github.com/libp2p/specs/tree/master) which are described in detail in
the [Lite](https://gitlab.com/lp2p/lite/) library.

The application itself requires the **IPv6** protocol to function. When you are behind a router you
might be able to switch to the **IPv6** protocol. Reason for this limitation is, that it is expected
to have a higher success rate when connecting to others peers.

### Implementation

The application always tries to connect to other relay peers, so that at least it can be reachable
from the outside via the **hole punching** mechanism. In the mobile context it often the case that
a peer is behind a stateful NAT.

The data exchange mechanism based on the **IPNS** idea (here it is called **PNS**). Each node has
an unique identifier (PeerId) where it can be found within the network. This identifier is also used
for marking the content via the **pns://** Uri.

Consequences and benefits of this approach are:

- Only the **pns** link is shared, all changing data within the server are reflected automatically
  by the client.
- The **pns** link published by the server is directly connected to its peer ID.
  That means, that a client can automatically find the server via the DHT, when it obtains the link.
- In order that clients can find the node via the DHT, the server has to connect to relay nodes near
  its peer ID (called swarm). The client then tries to connect to the server via those relay nodes.
- Shortcut for clients: Instead of doing a lookup in IPFS DHT for the **pns** link
  to receive the root CID object, it simply asks the server itself for the root CID object (which
  contains the content information)

### Conclusion

The current implementation of **Odin** is not compatible to **IPFS (kubo)**, due
to the fact that it does not publish any data to the IPFS DHT.

Therefore a regular IPFS node, will not find any **Odin** data.

With this in mind the, a client [Thor](https://gitlab.com/lp2p/thor) has been developed, which is
optimized for this behaviour.

## Links

[Privacy Policy](https://gitlab.com/lp2p/odin/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/lp2p/odin/-/blob/master/LICENSE)
