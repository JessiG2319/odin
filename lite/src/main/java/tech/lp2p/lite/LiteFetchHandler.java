package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.Lite;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Handler;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class LiteFetchHandler implements Handler {

    private final BlockStore blockStore;

    public LiteFetchHandler(@NotNull BlockStore blockStore) {
        this.blockStore = blockStore;
    }

    @Override
    public void protocol(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        if (data.length != 32) {
            throw new Exception("Invalid hash length");
        }

        Cid cid = new Cid(data);
        byte[] blockData = blockStore.getBlock(cid);
        if (blockData != null) {
            stream.response(blockData, Lite.GRACE_PERIOD);
        } else {
            stream.response(Utils.BYTES_EMPTY, Lite.DEFAULT_TIMEOUT);
        }
    }

    @Override
    public void terminated(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void fin(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

}
