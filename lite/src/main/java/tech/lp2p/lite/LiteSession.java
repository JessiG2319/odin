package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Acceptor;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Certificate;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Protocols;
import tech.lp2p.core.Responder;
import tech.lp2p.core.Session;
import tech.lp2p.dht.DhtKademlia;
import tech.lp2p.ident.IdentifyHandler;
import tech.lp2p.ident.IdentifyPushHandler;


public final class LiteSession implements Session {
    @NotNull
    private final BlockStore blockStore;
    @NotNull
    private final DhtKademlia dhtKademlia;
    @NotNull
    private final LiteHost host;
    @NotNull
    private final Responder responderLibp2p;
    @NotNull
    private final Responder responderLite = new Responder(new Protocols());

    public LiteSession(@NotNull BlockStore blockStore,
                       @NotNull PeerStore peerStore,
                       @NotNull LiteHost host) {
        this.blockStore = blockStore;
        this.host = host;
        this.dhtKademlia = new DhtKademlia(this, peerStore);

        Protocols libp2p = new Protocols();
        libp2p.put(Protocol.MULTISTREAM_PROTOCOL, new LiteStreamHandler());
        libp2p.put(Protocol.IDENTITY_PROTOCOL, new IdentifyHandler(this));
        if (Protocol.IDENTITY_PUSH_PROTOCOL.enabled()) {
            libp2p.put(Protocol.IDENTITY_PUSH_PROTOCOL, new IdentifyPushHandler());
        }

        this.responderLibp2p = new Responder(libp2p);

    }

    @NotNull
    public Responder responder(@NotNull ALPN alpn) {
        switch (alpn) {
            case lite -> {
                return responderLite;
            }
            case libp2p -> {
                return responderLibp2p;
            }
        }
        throw new IllegalStateException();
    }

    @NotNull
    @Override
    public Keys keys() {
        return host.keys();
    }


    @NotNull
    public Protocols protocols(@NotNull ALPN alpn) {
        return responder(alpn).protocols();
    }

    @NotNull
    @Override
    public BlockStore blockStore() {
        return blockStore;
    }

    @NotNull
    @Override
    public PeerId self() {
        return host.self();
    }

    @Override
    @NotNull
    public Peeraddrs findPeeraddrs(@NotNull Key key, int timeout) {
        return dhtKademlia.findPeeraddrs(key, timeout);
    }

    @Override
    public void providers(@NotNull Key key, @NotNull Acceptor acceptor) {
        dhtKademlia.providers(key, acceptor);
    }


    @NotNull
    @Override
    public Certificate certificate() {
        return host.certificate();
    }

    @Override
    public void findClosestPeers(@NotNull Key key, @NotNull Acceptor acceptor) {
        dhtKademlia.findClosestPeers(key, acceptor);
    }

    @NotNull
    @Override
    public Peeraddrs bootstrap() {
        return host.bootstrap();
    }

    @NotNull
    @Override
    public Peeraddrs peeraddrs() {
        return new Peeraddrs(); // nothing to publish
    }


    @NotNull
    public String agent() {
        return host.agent();
    }
}

