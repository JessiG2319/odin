package tech.lp2p.lite;

import org.jetbrains.annotations.NotNull;

import tech.lp2p.Lite;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Payload;
import tech.lp2p.core.Request;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class LitePullHandler implements Handler {
    private final LiteServer liteServer;

    LitePullHandler(@NotNull LiteServer liteServer) {
        this.liteServer = liteServer;
    }

    @Override
    public void protocol(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void data(Stream stream, byte[] data) {
        Envelope envelope = liteServer.envelope(
                new Request(Payload.toPayload(data), stream.connection().remotePeerId()));
        if (envelope != null) {
            stream.response(Envelope.toArray(envelope), Lite.ENVELOPE_TIMEOUT);
        } else {
            stream.response(Utils.BYTES_EMPTY, Lite.DEFAULT_TIMEOUT);
        }

    }

    @Override
    public void terminated(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }

    @Override
    public void fin(Stream stream) {
        throw new IllegalStateException("should never be invoked here");
    }
}
