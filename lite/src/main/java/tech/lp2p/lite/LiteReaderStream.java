package tech.lp2p.lite;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;


public class LiteReaderStream extends InputStream {

    private final LiteReader liteReader;
    ByteString buffer = null;
    private int positionBuffer = 0;

    public LiteReaderStream(@NotNull LiteReader liteReader) {
        this.liteReader = liteReader;
    }


    @Override
    public int available() {
        long size = liteReader.getSize();
        return (int) size;
    }

    void loadNextData() throws Exception {
        positionBuffer = 0;
        buffer = liteReader.loadNextData();
    }

    @Override
    public int read() throws IOException {

        try {
            if (buffer == null) { // initial the buffer is null
                loadNextData();
            }
            if (buffer.isEmpty()) {
                return -1;
            }
            if (positionBuffer < buffer.size()) {
                return buffer.byteAt(positionBuffer++) & 0xFF;
            } else {
                loadNextData();
                if (buffer.isEmpty()) {
                    return -1;
                }
                if (positionBuffer < buffer.size()) {
                    return buffer.byteAt(positionBuffer++) & 0xFF;
                }
                return -1;
            }
        } catch (Throwable e) {
            throw new IOException(e);
        }
    }
}
