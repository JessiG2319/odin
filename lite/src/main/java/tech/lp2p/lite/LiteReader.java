package tech.lp2p.lite;

import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagFetch;
import tech.lp2p.dag.DagReader;
import tech.lp2p.dag.DagService;
import tech.lp2p.proto.Merkledag;

public record LiteReader(@NotNull DagReader dagReader, @NotNull DagFetch dagFetch) {

    @NotNull
    public static LiteReader getReader(@NotNull Connection connection, @NotNull Cid cid)
            throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return LiteReader.createReader(fetchManager, cid);
    }

    @NotNull
    public static LiteReader getReader(@NotNull Session session, @NotNull Cid cid) throws Exception {
        return LiteReader.createReader(new LiteLocalFetch(session), cid);
    }

    public static LiteReader createReader(@NotNull DagFetch dagFetch, @NotNull Cid cid)
            throws Exception {

        Merkledag.PBNode top = DagService.getNode(dagFetch, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top);

        return new LiteReader(dagReader, dagFetch);
    }

    @NotNull
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(dagFetch);
    }

    public void seek(int position) throws Exception {
        dagReader.seek(dagFetch, position);
    }

    public long getSize() {
        return this.dagReader.size();
    }
}
