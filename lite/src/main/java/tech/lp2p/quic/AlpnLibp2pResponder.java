package tech.lp2p.quic;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Responder;


public record AlpnLibp2pResponder(Responder responder,
                                  Libp2pState streamState) implements StreamHandler {


    public static AlpnLibp2pResponder create(@NotNull Responder responder) {
        return new AlpnLibp2pResponder(responder, new Libp2pState(responder));
    }

    @Override
    public void terminated(Stream stream) {
        streamState.reset();
        responder.terminated(stream, streamState.protocol());
    }

    @Override
    public void fin(Stream stream) {
        streamState.reset();
        responder.fin(stream, streamState.protocol());
    }

    @Override
    public void throwable(Throwable throwable) {
        streamState.reset();
    }


    static class Libp2pState extends StreamState {
        private final Responder responder;
        Protocol protocol = null;

        Libp2pState(Responder responder) {
            this.responder = responder;
        }

        @Nullable
        public Protocol protocol() {
            return protocol;
        }

        public void accept(Stream stream, byte[] frame) throws Exception {
            if (frame.length > 0) {
                if (isProtocol(frame)) {
                    String name = new String(frame, 0, frame.length - 1);
                    protocol = Protocol.name(ALPN.libp2p, name);
                    responder.protocol(stream, protocol);
                } else {
                    responder.data(stream, protocol, frame);
                }
            }
        }
    }
}
