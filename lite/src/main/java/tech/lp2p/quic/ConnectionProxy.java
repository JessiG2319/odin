package tech.lp2p.quic;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;


public interface ConnectionProxy {

    InetSocketAddress remoteAddress();

    void parsePackets(long timeReceived, ByteBuffer data);


    void terminate();
}
