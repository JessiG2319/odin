package tech.lp2p.quic;


import java.util.function.Consumer;

record PacketSend(Packet packet,
                  Consumer<Packet> packetLostCallback,
                  Consumer<Packet> packetReceivedCallback) {
}

