package tech.lp2p.quic;

@SuppressWarnings("unused")
interface ServerConnectionRegistry {

    void registerConnection(ConnectionProxy connection, byte[] connectionId);

    void deregisterConnection(ConnectionProxy connection, byte[] connectionId);

    void registerAdditionalConnectionId(byte[] currentConnectionId, byte[] newConnectionId);

    void deregisterConnectionId(byte[] connectionId);
}
