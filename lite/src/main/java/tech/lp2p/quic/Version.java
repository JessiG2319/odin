package tech.lp2p.quic;


import java.nio.ByteBuffer;

public record Version(int versionId) {

    public static final Version QUIC_version_1 = new Version(0x00000001);
    public static final Version QUIC_version_2 = new Version(0x709a50c4);

    public static Version parse(int input) {
        return new Version(input);
    }

    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(versionId);
        return buffer.array();
    }

    public boolean isV2() {
        return versionId == 0x709a50c4;
    }

    public boolean isV1V2() {
        return versionId == 0x00000001 || versionId == 0x709a50c4;
    }


}
