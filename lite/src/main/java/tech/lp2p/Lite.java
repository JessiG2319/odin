package tech.lp2p;


import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Acceptor;
import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.HopConnect;
import tech.lp2p.core.Host;
import tech.lp2p.core.Info;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Network;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Payload;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Progress;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Request;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.crypto.Ed25519Sign;
import tech.lp2p.dag.DagInputStream;
import tech.lp2p.dag.DagResolver;
import tech.lp2p.dag.DagService;
import tech.lp2p.dag.DagStream;
import tech.lp2p.lite.LiteFetchManager;
import tech.lp2p.lite.LiteHost;
import tech.lp2p.lite.LiteProgressStream;
import tech.lp2p.lite.LiteReader;
import tech.lp2p.lite.LiteReaderStream;
import tech.lp2p.lite.LiteServer;
import tech.lp2p.lite.LiteService;
import tech.lp2p.lite.LiteSession;
import tech.lp2p.quic.ConnectionBuilder;
import tech.lp2p.relay.RelayDialer;
import tech.lp2p.relay.RelayService;


public final class Lite {

    public static final int CHUNK_BASIS = 1024;
    public static final int CHUNK_DEFAULT = 4 * Lite.CHUNK_BASIS;
    public static final int CHUNK_DATA = CHUNK_BASIS * 50;
    public static final int GRACE_PERIOD = 15;
    public static final int DHT_TABLE_SIZE = 200;
    public static final int DHT_ALPHA = 30;
    public static final int DHT_CONCURRENCY = 5;
    public static final int DHT_REQUEST_TIMEOUT = 5; // in seconds
    public static final int DHT_MESSAGE_TIMEOUT = 5; // in seconds
    public static final int DEFAULT_TIMEOUT = 1; // in seconds
    public static final int ENVELOPE_TIMEOUT = 2; // in seconds
    public static final int CONNECT_TIMEOUT = 5; // in seconds
    public static final int RELAY_CONNECT_TIMEOUT = 30; // in seconds

    private static final long LIMIT_BYTES = 1024;
    private static final int LIMIT_DURATION = 15 * 50; // in seconds [15 min]

    @NotNull
    private final LiteHost host;

    public Lite(@NotNull Settings settings) throws Exception {
        Keys keys = settings.keys();
        Objects.requireNonNull(keys);
        String agent = settings.agent();
        Objects.requireNonNull(agent);
        Peeraddrs bootstrap = settings.bootstrap();
        Objects.requireNonNull(bootstrap);

        this.host = new LiteHost(keys, bootstrap, agent);
    }

    public static Keys generateKeys() throws Exception {
        Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
        return new Keys(PeerId.create(keyPair.getPublicKey()),
                keyPair.getPrivateKey());
    }


    public static void pushEnvelope(@NotNull Connection connection,
                                    @NotNull Envelope envelope) throws Exception {
        LiteService.pushEnvelope(connection, envelope);
    }

    @NotNull
    public static Envelope createEnvelope(@NotNull Host host,
                                          @NotNull Payload type,
                                          @NotNull Cid cid) throws Exception {
        byte[] payload = Payload.unsignedEnvelopePayload(type, cid);
        byte[] signature = host.keys().sign(payload);
        return new Envelope(host.self(), cid, type, signature);
    }

    @NotNull
    public static String fetchText(@NotNull Connection connection,
                                   @NotNull Cid cid,
                                   @NotNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(connection, outputStream, cid, progress);
            return outputStream.toString();
        }
    }

    @NotNull
    public static String fetchText(@NotNull Session session, @NotNull Cid cid) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid);
            return outputStream.toString();
        }
    }


    public static byte[] fetchData(@NotNull Connection connection,
                                   @NotNull Cid cid,
                                   @NotNull Progress progress) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(connection, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }


    public static byte[] fetchData(@NotNull Session session, @NotNull Cid cid) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid);
            return outputStream.toByteArray();
        }
    }


    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NotNull Connection connection,
                                           @NotNull OutputStream outputStream,
                                           @NotNull Cid cid,
                                           @NotNull Progress progress) throws Exception {

        long totalRead = 0L;
        int remember = 0;

        LiteReader liteReader = LiteReader.getReader(connection, cid);
        long size = liteReader.getSize();

        do {
            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();

            if (size > 0) {
                int percent = (int) ((totalRead * 100.0f) / size);
                if (remember < percent) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }

        } while (true);
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public static void fetchToOutputStream(@NotNull Session session,
                                           @NotNull OutputStream outputStream,
                                           @NotNull Cid cid)
            throws Exception {

        LiteReader liteReader = LiteReader.getReader(session, cid);
        do {
            ByteString buffer = liteReader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }

    @NotNull
    public static Peeraddrs reservationPeeraddrs(@NotNull Server server) {
        return server.reservationPeeraddrs();
    }

    @Nullable
    public static Envelope pullEnvelope(@NotNull Connection connection,
                                        @NotNull Payload payload) throws Exception {
        return LiteService.pullEnvelope(connection, payload);
    }

    @NotNull
    public static Fid storeFile(@NotNull Session session, @NotNull File file) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(session, file.getName(), inputStream);
        }
    }

    // data is limited to Lite.CHUNK_SIZE
    @NotNull
    public static Raw storeData(@NotNull Session session, byte[] data) {
        return DagService.createRaw(session, data);
    }

    // data is limited to Lite.CHUNK_SIZE
    @NotNull
    public static Raw storeText(@NotNull Session session, @NotNull String data) {
        return DagService.createRaw(session, data.getBytes());
    }

    @NotNull
    public static Fid storeInputStream(@NotNull Session session,
                                       @NotNull String name,
                                       @NotNull InputStream inputStream,
                                       @NotNull Progress progress, long size) throws Exception {

        return DagStream.readInputStream(session, name,
                new DagInputStream(inputStream, progress, size));

    }

    @NotNull
    public static Fid storeInputStream(@NotNull Session session,
                                       @NotNull String name,
                                       @NotNull InputStream inputStream)
            throws Exception {
        return DagStream.readInputStream(session, name, new DagInputStream(inputStream, 0));
    }


    @NotNull
    public static InputStream getInputStream(@NotNull Connection connection, @NotNull Cid cid)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(connection, cid);
        return new LiteReaderStream(liteReader);
    }


    @NotNull
    public static InputStream getInputStream(@NotNull Session session, @NotNull Cid cid)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(session, cid);
        return new LiteReaderStream(liteReader);
    }

    @NotNull
    public static InputStream getInputStream(@NotNull Connection connection, @NotNull Cid cid,
                                             @NotNull Progress progress)
            throws Exception {
        LiteReader liteReader = LiteReader.getReader(connection, cid);
        return new LiteProgressStream(liteReader, progress);

    }

    public static void fetchToFile(@NotNull Session session, @NotNull File file, @NotNull Cid cid)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid);
        }
    }

    public static void fetchToFile(@NotNull Connection connection, @NotNull File file,
                                   @NotNull Cid cid,
                                   @NotNull Progress progress) throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(connection, fileOutputStream, cid, progress);
        }
    }

    // has the session block storage the cid block
    public static boolean hasBlock(@NotNull Session session, @NotNull Cid cid) {
        return session.blockStore().hasBlock(cid);
    }


    // remove the cid block (add all links blocks recursively) from the session block storage
    public static void removeBlocks(@NotNull Session session, @NotNull Cid cid) throws Exception {
        DagStream.removeBlocks(session, cid);
    }

    // returns all blocks of the cid from the session block storage,
    // If the cid block contains links, also the links cid blocks are returned (recursive)
    @NotNull
    public static List<Cid> blocks(@NotNull Session session, @NotNull Cid cid) throws Exception {
        return DagStream.blocks(session, cid);
    }


    @NotNull
    public static Dir removeFromDirectory(@NotNull Session session, @NotNull Dir dir,
                                          @NotNull Dir child) throws Exception {
        return DagStream.removeFromDirectory(session, dir.cid(), child);
    }

    @NotNull
    public static Dir removeFromDirectory(@NotNull Session session, @NotNull Dir dir,
                                          @NotNull Fid child) throws Exception {
        return DagStream.removeFromDirectory(session, dir.cid(), child);
    }

    @NotNull
    public static Dir addToDirectory(@NotNull Session session, @NotNull Dir dir,
                                     @NotNull Dir child) throws Exception {
        return DagStream.addToDirectory(session, dir.cid(), child);
    }

    @NotNull
    public static Dir addToDirectory(@NotNull Session session, @NotNull Dir dir,
                                     @NotNull Fid child) throws Exception {
        return DagStream.addToDirectory(session, dir.cid(), child);
    }

    @NotNull
    public static Dir updateDirectory(@NotNull Session session, @NotNull Dir dir,
                                      @NotNull Fid child) throws Exception {
        return DagStream.updateDirectory(session, dir.cid(), child);
    }

    @NotNull
    public static Dir updateDirectory(@NotNull Session session, @NotNull Dir dir,
                                      @NotNull Dir child) throws Exception {
        return DagStream.updateDirectory(session, dir.cid(), child);
    }

    // Note: children can only be of Type Fid and Dir
    @NotNull
    public static Dir createDirectory(@NotNull Session session, @NotNull String name,
                                      @NotNull List<Info> children) {
        return DagStream.createDirectory(session, name, children);
    }

    @NotNull
    public static Dir createEmptyDirectory(@NotNull Session session, @NotNull String name) {
        return DagStream.createEmptyDirectory(session, name);
    }

    @NotNull
    public static Dir renameDirectory(@NotNull Session session, @NotNull Dir dir,
                                      @NotNull String name) throws Exception {
        return DagStream.renameDirectory(session, dir.cid(), name);
    }

    @NotNull
    public static Info info(@NotNull Session session, @NotNull Cid cid) throws Exception {
        return DagStream.info(session.blockStore(), cid);
    }

    public static boolean hasChild(@NotNull Connection connection, @NotNull Dir dir,
                                   @NotNull String name) throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return DagStream.hasChild(fetchManager, dir, name);
    }

    public static boolean hasChild(@NotNull Session session, @NotNull Dir dir, @NotNull String name)
            throws Exception {
        return DagStream.hasChild(session.blockStore(), dir, name);
    }


    @NotNull
    public static List<Raw> raws(@NotNull Session session, @NotNull Fid fid) throws Exception {
        return DagStream.raws(session.blockStore(), fid);
    }

    @NotNull
    public static List<Info> childs(@NotNull Session session, @NotNull Dir dir) throws Exception {
        return DagStream.childs(session.blockStore(), dir);
    }


    public static void provideKey(@NotNull Server server, @NotNull Key key,
                                  @NotNull Acceptor acceptor) {
        server.provideKey(key, acceptor);
    }

    public static void providers(@NotNull Session session, @NotNull Key key,
                                 @NotNull Acceptor acceptor) {
        session.providers(key, acceptor);
    }

    @NotNull
    public static Peeraddrs findPeeraddrs(@NotNull Session session,
                                          @NotNull PeerId peerId, int timeout) {
        return session.findPeeraddrs(peerId.createKey(), timeout);
    }

    public static void findClosestPeeraddrs(@NotNull Session session,
                                            @NotNull PeerId peerId,
                                            @NotNull Acceptor acceptor) {
        session.findClosestPeers(peerId.createKey(), acceptor);
    }


    @NotNull
    public static Connection dial(@NotNull Session session,
                                  @NotNull Peeraddr peeraddr,
                                  @NotNull Parameters parameters)
            throws ConnectException, InterruptedException, TimeoutException {
        return ConnectionBuilder.connect(session, peeraddr, parameters,
                session.responder(parameters.alpn()));
    }


    @Nullable
    public static HopConnect hopConnect(@NotNull Session session,
                                        @NotNull PeerId peerId,
                                        @NotNull Parameters parameters,
                                        @NotNull Consumer<Peeraddr> tries,
                                        @NotNull Consumer<Peeraddr> failures,
                                        int timeout) {
        return RelayDialer.hopConnect(session, peerId, parameters, tries, failures, timeout);
    }

    @NotNull
    public static Connection hopConnect(@NotNull Session session,
                                        @NotNull Peeraddr relay,
                                        @NotNull PeerId target,
                                        @NotNull Parameters parameters) throws ConnectException {
        return RelayService.hopConnect(session, relay, target, parameters);
    }

    @NotNull
    public static Parameters createParameters() {
        return createParameters(false);
    }

    @NotNull
    public static Parameters createParameters(boolean keepAlive) {
        return Parameters.create(ALPN.lite, keepAlive);
    }

    @NotNull
    public static Cid decodeCid(@NotNull String cid) throws Exception {
        return Cid.decodeCid(cid);
    }

    @NotNull
    public static PeerId decodePeerId(@NotNull String pid) throws Exception {
        return PeerId.decodePeerId(pid);
    }

    // Utility function, resolves a root Cid object till the path of links is reached
    @NotNull
    public static Info resolvePath(@NotNull Connection connection, @NotNull Cid root,
                                   @NotNull List<String> path) throws Exception {
        LiteFetchManager fetchManager = new LiteFetchManager(connection);
        return DagResolver.resolvePath(fetchManager, root, path);
    }

    @NotNull
    public static Info resolvePath(@NotNull Session session, @NotNull Cid root,
                                   @NotNull List<String> path) throws Exception {
        return DagResolver.resolvePath(session.blockStore(), root, path);
    }

    // return true, when it has reservations
    public static boolean hasReservations(@NotNull Server server) {
        return server.hasReservations();
    }

    // this function returns all the valid reservations
    @NotNull
    public static Set<Reservation> reservations(@NotNull Server server) {
        return server.reservations();
    }


    // this function does the reservation [it is bound to a server]
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    public static void hopReserve(@NotNull Server server, int maxReservation, int timeout) {
        server.hopReserve(maxReservation, timeout);
    }

    // this function does the reservation [it is bound to a server]
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    // Note: this function is used for testing (though it still adds it to the internal
    // reservation list, when succeeds)

    @NotNull
    static Reservation hopReserve(@NotNull Server server, @NotNull Peeraddr relay) throws Exception {
        return server.hopReserve(relay);
    }


    @NotNull
    static Reservation refreshReservation(@NotNull Server server,
                                          @NotNull Reservation reservation) throws Exception {
        return server.refreshReservation(reservation);
    }


    static void closeReservation(@NotNull Server server, @NotNull Reservation reservation) {
        server.closeReservation(reservation);
    }


    @NotNull
    public static Settings createSettings(@NotNull Keys keys,
                                          @NotNull Peeraddrs bootstrap,
                                          @NotNull String agent) {
        return new Settings(keys, bootstrap, agent);
    }


    @NotNull
    public static Cid rawCid(@NotNull String string) {
        // not the cid is not stored in the block store
        return DagService.createRaw(string.getBytes(StandardCharsets.UTF_8));
    }

    @NotNull
    public static Server.Settings createServerSettings(int port) {
        return new Server.Settings(port, new Limit(LIMIT_BYTES, LIMIT_DURATION));
    }

    @NotNull
    public static Server.Settings createServerSettings(int port, Limit limit) {
        return new Server.Settings(port, limit);
    }

    @NotNull
    public static Peeraddr createPeeraddr(PeerId peerId, InetAddress inetAddress, int port) {
        return Peeraddr.create(peerId, inetAddress.getAddress(), port);
    }

    @NotNull
    public static Peeraddr createPeeraddr(String peerId, String address, int port) throws Exception {
        return createPeeraddr(decodePeerId(peerId), InetAddress.getByName(address), port);
    }

    // Reservation feature is only possible when a public Inet6Address is available
    public static boolean reservationFeaturePossible() {
        return !Network.publicAddresses().isEmpty();
    }

    @NotNull
    public Keys keys() {
        return host.keys();
    }

    @NotNull
    public PeerId self() {
        return host.self();
    }

    @NotNull
    public Session createSession(@NotNull BlockStore blockStore, @NotNull PeerStore peerStore) {
        return new LiteSession(blockStore, peerStore, host);
    }

    @NotNull
    public Server startServer(@NotNull Server.Settings settings,
                              @NotNull BlockStore blockStore,
                              @NotNull PeerStore peerStore,
                              @NotNull Consumer<Peeraddr> connectionGain,
                              @NotNull Consumer<Peeraddr> connectionLost,
                              @NotNull Consumer<Reservation> reservationGain,
                              @NotNull Consumer<Reservation> reservationLost,
                              @NotNull Function<PeerId, Boolean> isGated,
                              @NotNull Function<Request, Envelope> envelopeSupplier,
                              @NotNull Consumer<Envelope> envelopeConsumer) {


        return LiteServer.startServer(settings, host, blockStore, peerStore,
                connectionGain, connectionLost, reservationGain, reservationLost, isGated,
                envelopeSupplier, envelopeConsumer);
    }

    public record Settings(@NotNull Keys keys,
                           @NotNull Peeraddrs bootstrap,
                           @NotNull String agent) {
    }
}
