package tech.lp2p.dag;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.proto.Merkledag;

public record DagStage(@NotNull Merkledag.PBNode node, @NotNull AtomicInteger index) {

    public static DagStage createDagStage(@NotNull Merkledag.PBNode node) {
        return new DagStage(node, new AtomicInteger(0));
    }

    public void incrementIndex() {
        index.incrementAndGet();
    }


    public void setIndex(int value) {
        index.set(value);
    }

}
