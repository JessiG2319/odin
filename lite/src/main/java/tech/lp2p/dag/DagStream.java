package tech.lp2p.dag;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Dir;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Info;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Session;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;


public interface DagStream {


    @NotNull
    static Info info(@NotNull BlockStore blockStore, @NotNull Cid cid) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, cid);
        Objects.requireNonNull(node);
        return info(cid, node);
    }

    @NotNull
    static Info info(Cid cid, Merkledag.PBNode node) throws Exception {
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() == Unixfs.Data.DataType.Directory) {
            return new Dir(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.File) {
            return new Fid(cid, unixData.getFilesize(), unixData.getName());
        }
        if (unixData.getType() == Unixfs.Data.DataType.Raw) {
            return new Raw(cid, unixData.getFilesize());
        }
        throw new IllegalStateException();
    }

    @NotNull
    static Dir createEmptyDirectory(@NotNull Session session, @NotNull String name) {
        return DagService.createEmptyDirectory(session, name);
    }

    @NotNull
    static Dir addToDirectory(@NotNull Session session,
                              @NotNull Cid directory,
                              @NotNull Info info) throws Exception {

        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.addChild(session, dirNode, info);

    }

    @NotNull
    static Dir updateDirectory(@NotNull Session session, @NotNull Cid directory,
                               @NotNull Info info) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.updateDirectory(session, dirNode, info);

    }

    @NotNull
    static Dir createDirectory(@NotNull Session session,
                               @NotNull String name,
                               @NotNull List<Info> links) {
        return DagService.createDirectory(session, name, links);
    }

    @NotNull
    static Dir renameDirectory(@NotNull Session session,
                               @NotNull Cid directory,
                               @NotNull String name) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.renameDirectory(session, dirNode, name);
    }

    @NotNull
    static Dir removeFromDirectory(@NotNull Session session, @NotNull Cid directory,
                                   @NotNull Info info) throws Exception {
        byte[] block = session.blockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = DagService.node(block);
        Objects.requireNonNull(dirNode);
        return DagService.removeChild(session, dirNode, info);
    }

    @NotNull
    static List<Cid> blocks(@NotNull Session session, @NotNull Cid cid) throws Exception {
        List<Cid> result = new ArrayList<>();

        byte[] block = session.blockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.createCid(Hash.toHash(link.getHash().toByteArray()));
                result.add(child);
                result.addAll(blocks(session, child));
            }
        }
        return result;
    }

    static void removeBlocks(@NotNull Session session, @NotNull Cid cid) throws Exception {
        byte[] block = session.blockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = DagService.node(block);
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.createCid(Hash.toHash(link.getHash().toByteArray()));
                removeBlocks(session, child);
            }
            session.blockStore().deleteBlock(cid);
        }
    }

    static List<Info> childs(@NotNull BlockStore blockStore, @NotNull Dir dir) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);

        List<Info> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            String name = link.getName();
            Merkledag.PBLinkType type = link.getType();
            Cid cid = Cid.createCid(Hash.toHash(link.getHash().toByteArray()));
            if (type != null) {
                switch (type) {
                    case Dir -> result.add(new Dir(cid, link.getTsize(), name));
                    case File -> result.add(new Fid(cid, link.getTsize(), name));
                }
            }
        }
        return result;
    }

    static List<Raw> raws(@NotNull BlockStore blockStore, @NotNull Fid fid) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, fid.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        if (unixData.getType() != Unixfs.Data.DataType.File) {
            throw new IllegalStateException("invalid node");
        }
        List<Raw> result = new ArrayList<>();
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            result.add(new Raw(Cid.createCid(
                    Hash.toHash(link.getHash().toByteArray())), link.getTsize()));
        }
        return result;
    }

    static boolean hasChild(@NotNull DagFetch dagFetch, @NotNull Dir dir,
                            @NotNull String name) throws Exception {
        Merkledag.PBNode node = DagService.getNode(dagFetch, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    static boolean hasChild(@NotNull BlockStore blockStore, @NotNull Dir dir,
                            @NotNull String name) throws Exception {
        Merkledag.PBNode node = DagService.getNode(blockStore, dir.cid());
        Objects.requireNonNull(node);
        Unixfs.Data unixData = DagService.info(node);
        DagService.checkDirectory(unixData);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NotNull
    static Fid readInputStream(@NotNull Session session,
                               @NotNull String name,
                               @NotNull DagInputStream dagInputStream) throws Exception {

        return DagService.createFromStream(session, name, dagInputStream);
    }

}
