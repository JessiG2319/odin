package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Objects;

import tech.lp2p.utils.Utils;

public record Payload(int type) implements Serializable {

    public static byte[] unsignedEnvelopePayload(Payload payload, Cid cid) {

        byte[] data = cid.hash();

        int payloadTypeLength = Utils.unsignedVariantSize(payload.type());
        int payloadLength = Utils.unsignedVariantSize(data.length);

        ByteBuffer buffer = ByteBuffer.allocate(payloadTypeLength +
                payloadLength + data.length);

        Utils.writeUnsignedVariant(buffer, payload.type());
        Utils.writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }

    @NotNull
    public static Payload toPayload(byte[] data) {
        Objects.requireNonNull(data, "data can not be null");

        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(data)) {
            try (ObjectInputStream objectInputStream =
                         new ObjectInputStream(inputStream)) {
                return (Payload) objectInputStream.readObject();
            }

        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static byte[] toArray(Payload payload) {
        Objects.requireNonNull(payload, "payload can not be null");

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
                objectOutputStream.writeObject(payload);
                return outputStream.toByteArray();
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }
}
