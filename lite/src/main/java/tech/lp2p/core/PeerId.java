package tech.lp2p.core;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;

import tech.lp2p.crypto.Ed25519Verify;
import tech.lp2p.proto.Crypto;
import tech.lp2p.utils.Base58;
import tech.lp2p.utils.Utils;

// hash is always (32 bit) and it is a Ed25519 public key
public record PeerId(byte[] hash) implements Serializable {
    @NotNull
    public static PeerId create(byte[] hash) {
        Objects.requireNonNull(hash, "hash can not be null");
        Utils.checkArgument(hash.length, 32, "hash size must be 32");
        return new PeerId(hash);
    }

    @Nullable
    public static PeerId parse(byte[] raw) {
        return parse(CodedInputStream.newInstance(raw));
    }


    @NotNull
    public static PeerId decodePeerId(@NotNull String name) throws Exception {

        if (name.startsWith("Qm") || name.startsWith("1")) {
            // base58 encoded sha256 or identity multihash
            CodedInputStream codedInputStream = CodedInputStream.newInstance(Base58.decode(name));

            int type = codedInputStream.readRawVarint32();
            int len = codedInputStream.readRawVarint32();

            byte[] hash = codedInputStream.readRawBytes(len);
            Utils.checkTrue(codedInputStream.isAtEnd(), "still data available");

            if (type == Multihash.SHA2_256) {
                if (hash.length != 32) {
                    throw new IllegalStateException("Incorrect hash length: " + hash.length);
                }
                // TODO [medium] only support Multihash.ID)
                return PeerId.create(hash);
            } else if (type == Multihash.ID) {
                return PeerId.create(
                        Crypto.PublicKey.parseFrom(hash).getData().toByteArray());
            }

            throw new IllegalStateException("not supported multihash");

        }

        byte[] data = Multibase.decode(name);

        CodedInputStream codedInputStream = CodedInputStream.newInstance(data);
        codedInputStream.readRawVarint32(); // skip version
        codedInputStream.readRawVarint32(); // skip multicodec
        return Objects.requireNonNull(PeerId.parse(codedInputStream));
    }

    @Nullable
    public static PeerId parse(CodedInputStream codedInputStream) {
        try {
            int type = codedInputStream.readRawVarint32();
            int len = codedInputStream.readRawVarint32();

            byte[] hash = codedInputStream.readRawBytes(len);
            if (!codedInputStream.isAtEnd()) {
                return null;
            }
            if (type == Multihash.ID) {
                byte[] raw = Crypto.PublicKey.parseFrom(hash).getData().toByteArray();
                if (raw.length != 32) {
                    return null;
                }
                return PeerId.create(raw);
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
        }
        return null;
    }

    @NotNull
    public static PeerId toPeerId(byte[] data) {
        Objects.requireNonNull(data, "data can not be null");
        Utils.checkArgument(data.length, 32, "data size must be 32");
        return new PeerId(data);
    }

    public static byte[] toArray(PeerId peerId) {
        Objects.requireNonNull(peerId, "peerId can not be null");
        return peerId.hash();
    }

    public static byte[] multihash(PeerId peerId) {

        byte[] bytes = Crypto.PublicKey.newBuilder()
                .setData(ByteString.copyFrom(peerId.hash()))
                .setType(Crypto.KeyType.Ed25519).build().toByteArray();

        int typeLength = Utils.unsignedVariantSize(Multihash.ID);
        int hashLength = Utils.unsignedVariantSize(bytes.length);

        ByteBuffer buffer = ByteBuffer.allocate(typeLength + hashLength + bytes.length);
        Utils.writeUnsignedVariant(buffer, Multihash.ID);
        Utils.writeUnsignedVariant(buffer, bytes.length);
        buffer.put(bytes);
        return buffer.array();
    }

    public Key createKey() {
        return Key.convertKey(hash());
    }

    public void verify(byte[] data, byte[] signature) throws Exception {
        Ed25519Verify verifier = new Ed25519Verify(hash());
        verifier.verify(signature, data);
    }

    // https://docs.ipfs.tech/concepts/content-addressing/#cid-inspector
    // The default for CIDv1 is the case-insensitive base32, but use of the shorter base36 is
    // encouraged for IPNS names to ensure same text representation on subdomains.
    //
    @NotNull
    public String toBase36() {
        byte[] multihash = multihash(this);
        int versionLength = Utils.unsignedVariantSize(1);
        int codecLength = Utils.unsignedVariantSize(Multicodec.LIBP2P_KEY);
        ByteBuffer buffer = ByteBuffer.allocate(
                versionLength + codecLength + multihash.length);
        Utils.writeUnsignedVariant(buffer, 1);
        Utils.writeUnsignedVariant(buffer, Multicodec.LIBP2P_KEY);
        buffer.put(multihash);
        return Multibase.encode(Multibase.BASE36, buffer.array());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerId peer = (PeerId) o;
        return Arrays.equals(hash, peer.hash);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(hash()); // ok, checked, maybe opt
    }

    // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md
    // String representation
    // There are two ways to represent peer IDs in text: as a raw base58btc encoded multihash
    // (e.g., Qm..., 1...) and as a multibase encoded CID (e.g., bafz...). Libp2p is slowly
    // transitioning from the first (legacy) format to the second (new).
    //
    // Implementations MUST support parsing both forms of peer IDs. Implementations SHOULD display
    // peer IDs using the first (raw base58btc encoded multihash) format until the second format
    // is widely supported.
    //
    // Peer IDs encoded as CIDs must be encoded using CIDv1 and must use the libp2p-key multicodec
    // (0x72). By default, such peer IDs SHOULD be encoded in using the base32 multibase
    // (RFC4648, without padding).
    //
    // For reference, CIDs (encoded in text) have the following format
    //
    // <multibase-prefix><cid-version><multicodec><multihash>
    // [Future toBase32() instead of toBase58()]
    @NotNull
    public String toBase58() {
        return Base58.encode(multihash(this));
    }

}
