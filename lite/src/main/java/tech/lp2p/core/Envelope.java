package tech.lp2p.core;


import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public record Envelope(PeerId peerId, Cid cid, Payload payload,
                       byte[] signature) implements Serializable {

    @NotNull
    public static Envelope toEnvelope(byte[] data) {
        Objects.requireNonNull(data, "data can not be null");

        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(data)) {
            try (ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
                return (Envelope) objectInputStream.readObject();
            }

        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public static byte[] toArray(Envelope envelope) {
        Objects.requireNonNull(envelope, "envelope can not be null");

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream)) {
                objectOutputStream.writeObject(envelope);
                return outputStream.toByteArray();
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Envelope envelope = (Envelope) o;
        return Objects.equals(peerId, envelope.peerId) &&
                Objects.equals(cid, envelope.cid) &&
                Objects.equals(payload, envelope.payload) &&
                Arrays.equals(signature, envelope.signature);
    }
}
