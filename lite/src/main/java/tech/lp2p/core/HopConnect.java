package tech.lp2p.core;

public record HopConnect(Connection connection, Peeraddr relay) {
}
