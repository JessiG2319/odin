package tech.lp2p.core;

public record Limit(long data, int duration) { // duration is seconds
    public boolean limited() {
        return data != 0 || duration != 0;
    }

}
