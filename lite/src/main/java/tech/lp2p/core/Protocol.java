package tech.lp2p.core;

import java.net.UnknownServiceException;
import java.util.Objects;

import tech.lp2p.Lite;


public record Protocol(String name, ALPN alpn, int codec, long readDelimiter,
                       long responderDelimiter, boolean enabled) {

    // Libp2p protocols
    public static final Protocol MULTISTREAM_PROTOCOL = new Protocol("/multistream/1.0.0",
            ALPN.libp2p, 1, 0, 0, true);
    public static final Protocol DHT_PROTOCOL = new Protocol("/ipfs/kad/1.0.0",
            ALPN.libp2p, 2, 2 * Lite.CHUNK_DATA, 2 *
            Lite.CHUNK_DATA, false);
    public static final Protocol IDENTITY_PROTOCOL = new Protocol("/ipfs/id/1.0.0",
            ALPN.libp2p, 3, 8 * Lite.CHUNK_BASIS, Lite.CHUNK_BASIS, true); // signed IDs
    public static final Protocol RELAY_PROTOCOL_HOP = new Protocol("/libp2p/circuit/relay/0.2.0/hop",
            ALPN.libp2p, 4, Lite.CHUNK_DEFAULT, Lite.CHUNK_DEFAULT, true);
    public static final Protocol RELAY_PROTOCOL_STOP = new Protocol("/libp2p/circuit/relay/0.2.0/stop",
            ALPN.libp2p, 5, Lite.CHUNK_DEFAULT, Lite.CHUNK_DEFAULT, true);
    public static final Protocol IDENTITY_PUSH_PROTOCOL = new Protocol("/ipfs/id/push/1.0.0",
            ALPN.libp2p, 6, Lite.CHUNK_BASIS, 2 * Lite.CHUNK_DEFAULT, false); // signed IDs

    // Lite protocols
    public static final Protocol LITE_PUSH_PROTOCOL = new Protocol("/lite/push/1.0.0",
            ALPN.lite, 2, Lite.CHUNK_BASIS, Lite.CHUNK_BASIS, true);
    public static final Protocol LITE_PULL_PROTOCOL = new Protocol("/lite/pull/1.0.0",
            ALPN.lite, 1, Lite.CHUNK_BASIS, Lite.CHUNK_BASIS, true);
    public static final Protocol LITE_FETCH_PROTOCOL = new Protocol("/lite/fetch/1.0.0",
            ALPN.lite, 0, Lite.CHUNK_BASIS + Lite.CHUNK_DATA, Lite.CHUNK_BASIS, true);

    public static Protocol name(ALPN alpn, String protocol) throws UnknownServiceException {
        if (Objects.requireNonNull(alpn) == ALPN.libp2p) {
            return switch (protocol) {
                case "/multistream/1.0.0" -> MULTISTREAM_PROTOCOL;
                case "/ipfs/kad/1.0.0" -> DHT_PROTOCOL;
                case "/ipfs/id/1.0.0" -> IDENTITY_PROTOCOL;
                case "/ipfs/id/push/1.0.0" -> IDENTITY_PUSH_PROTOCOL;
                case "/libp2p/circuit/relay/0.2.0/hop" -> RELAY_PROTOCOL_HOP;
                case "/libp2p/circuit/relay/0.2.0/stop" -> RELAY_PROTOCOL_STOP;
                default -> throw new UnknownServiceException("unhandled protocol " + protocol);
            };
        }
        throw new UnknownServiceException("unhandled protocol " + protocol);

    }

    public static Protocol codec(ALPN alpn, int protocol) throws UnknownServiceException {
        if (Objects.requireNonNull(alpn) == ALPN.lite) {
            return switch (protocol) {
                case 2 -> LITE_PUSH_PROTOCOL;
                case 1 -> LITE_PULL_PROTOCOL;
                case 0 -> LITE_FETCH_PROTOCOL;
                default -> throw new UnknownServiceException("unhandled protocol " + protocol);
            };
        }
        throw new UnknownServiceException("unhandled protocol " + protocol);
    }
}
