package tech.lp2p.core;


import java.util.Objects;

import tech.lp2p.crypto.Ed25519Sign;

// Note a peerId is always a public key (ed25519)
public record Keys(PeerId peerId, byte[] privateKey) {

    public byte[] sign(byte[] data) throws Exception {
        Objects.requireNonNull(data);
        Ed25519Sign signer = new Ed25519Sign(privateKey);
        return signer.sign(data);
    }
}

