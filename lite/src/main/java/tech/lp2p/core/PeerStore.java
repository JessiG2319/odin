package tech.lp2p.core;


import java.util.List;

public interface PeerStore {

    List<Peeraddr> peeraddrs(int limit);

    void storePeeraddr(Peeraddr peeraddr);

    void removePeeraddr(Peeraddr peeraddr);
}
