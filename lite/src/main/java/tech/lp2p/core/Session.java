package tech.lp2p.core;

import org.jetbrains.annotations.NotNull;


public interface Session extends Routing, Host {

    // returns the block store where all data is stored
    @NotNull
    BlockStore blockStore();

    @NotNull
    Certificate certificate();
}

