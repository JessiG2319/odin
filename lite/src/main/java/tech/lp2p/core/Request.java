package tech.lp2p.core;

public record Request(Payload payload, PeerId peerId) {
}
