package tech.lp2p.core;

import tech.lp2p.quic.Settings;


// todo remove validatePeerId
public record Parameters(ALPN alpn, boolean keepAlive, int initialMaxData, boolean validatePeerId) {

    public static Parameters create(ALPN alpn, boolean keepAlive) {
        return new Parameters(alpn, keepAlive, Settings.INITIAL_MAX_DATA, true);
    }

    public static Parameters create(ALPN alpn) {
        return new Parameters(alpn, false, Settings.INITIAL_MAX_DATA, true);
    }

    public static Parameters create(ALPN alpn, boolean keepAlive, boolean validatePeerId) {
        return new Parameters(alpn, keepAlive, Settings.INITIAL_MAX_DATA, validatePeerId);
    }
}
