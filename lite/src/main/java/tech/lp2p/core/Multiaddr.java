package tech.lp2p.core;

import com.google.protobuf.CodedInputStream;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.OutputStream;

import tech.lp2p.utils.Utils;

// https://github.com/multiformats/multicodec/blob/master/table.csv (see multiaddr)
record Multiaddr(int code, int size, String type) {

    static final Multiaddr IP4 = new Multiaddr(4, 32, "ip4");
    static final Multiaddr IP6 = new Multiaddr(41, 128, "ip6");
    static final Multiaddr UDP = new Multiaddr(273, 16, "udp");
    static final Multiaddr P2P = new Multiaddr(421, -1, "p2p");
    static final Multiaddr QUICV1 = new Multiaddr(461, 0, "quic-v1");

    @Nullable
    static Multiaddr lookup(int code) {
        return switch (code) {
            case 4 -> IP4;
            case 41 -> IP6;
            case 273 -> UDP;
            case 421 -> P2P;
            case 461 -> QUICV1;
            default -> null;
        };
    }


    private static void putUvarint(byte[] buf, long x) {
        int i = 0;
        while (x >= 0x80) {
            buf[i] = (byte) (x | 0x80);
            x >>= 7;
            i++;
        }
        buf[i] = (byte) x;
    }


    static void encodeProtocol(Multiaddr multiaddr, OutputStream out) throws IOException {
        int code = multiaddr.code;
        byte[] varint = new byte[(32 - Integer.numberOfLeadingZeros(code) + 6) / 7];
        putUvarint(varint, code);
        out.write(varint);
    }

    static void encodePart(Integer port, OutputStream outputStream) throws IOException {
        int x = port;
        outputStream.write(new byte[]{(byte) (x >> 8), (byte) x});
    }

    static void encodePart(byte[] address, OutputStream outputStream) throws IOException {
        outputStream.write(address);
    }

    @Nullable
    static Peeraddr parseAddress(CodedInputStream in, PeerId peerId) {

        boolean isFirst = true;
        byte[] address = null;
        int port = 0;
        try {
            while (!in.isAtEnd()) {
                Multiaddr multiaddr = Multiaddr.lookup(in.readRawVarint32());
                if (multiaddr == null) {
                    return null;
                }
                if (isFirst) {
                    // check if is valid and supported
                    if (!multiaddr.type().startsWith("ip")) { // ip4 or ip6
                        return null;
                    }
                    isFirst = false;
                }

                if (multiaddr.size() == 0) {
                    continue;
                }

                Object part = multiaddr.readPart(in);
                if (part == null) {
                    return null;
                }
                if (part instanceof byte[] inetAddress) {
                    address = inetAddress;
                } else if (part instanceof Integer) {
                    port = (Integer) part;
                } else {
                    break;
                }
            }
        } catch (Throwable throwable) {
            Utils.error(throwable); // should not occur
            return null;
        }
        // check if address has a port, when it is not a dnsAddr
        if (port > 0 && address != null) {
            return Peeraddr.create(peerId, address, port);
        }
        return null;
    }

    @Nullable
    private Object readPart(CodedInputStream in) {
        try {
            int sizeForAddress = sizeForAddress(in);
            byte[] buf;
            switch (code()) {
                case 4, 41 -> {
                    return in.readRawBytes(sizeForAddress);
                }
                case 273 -> {
                    int a = in.readRawByte() & 0xFF;
                    int b = in.readRawByte() & 0xFF;
                    return (a << 8) | b;
                }
                case 421 -> {
                    buf = in.readRawBytes(sizeForAddress);
                    return PeerId.parse(buf);
                }
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
        return null;
    }

    private int sizeForAddress(CodedInputStream in) throws IOException {
        if (size > 0)
            return size / 8;
        if (size == 0)
            return 0;
        return in.readRawVarint32();
    }
}
