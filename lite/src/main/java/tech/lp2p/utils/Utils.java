package tech.lp2p.utils;


import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import tech.lp2p.core.Progress;
import tech.lp2p.core.Protocol;
import tech.lp2p.proto.Lite;


public interface Utils {
    boolean DEBUG = false;
    boolean ERROR = false;

    byte[] BYTES_EMPTY = new byte[0];
    X509Certificate[] CERTIFICATES_EMPTY = new X509Certificate[0];

    static byte[] concat(byte[]... chunks) throws IllegalStateException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new IllegalStateException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }


    static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NotNull
    static DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }


    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new int[] {a, b}, new int[] {}, new int[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code int} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    static int[] concat(int[]... arrays) {
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        int pos = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }


    static int unsignedVariantSize(int value) {
        int remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    static void writeUnsignedVariant(ByteBuffer out, int value) {
        int remaining = value >>> 7;
        while (remaining != 0) {
            out.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        out.put((byte) (value & 0x7f));
    }


    static byte[] encode(@NotNull MessageLite message) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static byte[] encode(byte[] data) {
        int dataLength = unsignedVariantSize(data.length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + data.length);
        writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }

    static Lite.ProtoSelect.Protocol protocol(@NotNull Protocol protocol) {
        return switch (protocol.codec()) {
            case 0 -> Lite.ProtoSelect.Protocol.Fetch;
            case 1 -> Lite.ProtoSelect.Protocol.Pull;
            case 2 -> Lite.ProtoSelect.Protocol.Push;
            default -> throw new IllegalStateException();
        };
    }

    static byte[] createMessage(@NotNull Protocol protocol, byte[] data) {
        return encode(Lite.ProtoSelect.newBuilder()
                .setProtocol(protocol(protocol))
                .setData(ByteString.copyFrom(data))
                .build());
    }

    static byte[] encodeProtocols(Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    static byte[] encode(@NotNull MessageLite message, Protocol... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (Protocol protocol : protocols) {
                byte[] data = protocol.name().getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    @SuppressWarnings("UnusedReturnValue")
    static long copy(InputStream source, OutputStream sink) throws IOException {
        return source.transferTo(sink);
    }

    static void copy(@NotNull InputStream source, @NotNull OutputStream sink,
                     @NotNull Progress progress, long size) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (size > 0) {
                int percent = (int) ((nread * 100.0f) / size);
                if (percent > remember) {
                    remember = percent;
                    progress.setProgress(percent);
                }
            }
        }
    }


    static void debug(@NotNull String message) {
        if (DEBUG) {
            System.out.println(message);
        }
    }

    static void error(@NotNull String message) {
        if (ERROR) {
            System.err.println(message);
        }
    }

    static void error(@NotNull Throwable throwable) {
        if (ERROR) {
            throwable.printStackTrace(System.err);
        }
    }


    static void checkArgument(int val, int cmp, String message) {
        if (val != cmp) {
            throw new IllegalArgumentException(message);
        }
    }

    static void checkArgument(Object val, Object cmp, String message) {
        if (!Objects.equals(val, cmp)) {
            throw new IllegalArgumentException(message);
        }
    }

    static void checkTrue(boolean condition, String message) {
        if (!condition) {
            throw new IllegalStateException(message);
        }
    }

    static void runnable(@NotNull Runnable runnable, int timeout) {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.execute(runnable);
            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }
    }
}
