// Copyright 2017 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////

package tech.lp2p.crypto;

import java.security.SecureRandom;

/**
 * Provides secure randomness
 *
 * @since 1.0.0
 */
public final class Random {
    private static final ThreadLocal<SecureRandom> localRandom =
            ThreadLocal.withInitial(Random::newDefaultSecureRandom);

    private Random() {
    }

    private static SecureRandom newDefaultSecureRandom() {
        SecureRandom retrieval = new SecureRandom();
        retrieval.nextLong(); // force seeding
        return retrieval;
    }

    /**
     * Returns a random byte array of size {@code size}.
     */
    public static byte[] randBytes(int size) {
        byte[] rand = new byte[size];
        localRandom.get().nextBytes(rand);
        return rand;
    }


}
