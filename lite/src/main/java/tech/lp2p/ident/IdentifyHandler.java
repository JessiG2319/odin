package tech.lp2p.ident;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Handler;
import tech.lp2p.core.Host;
import tech.lp2p.core.Protocol;
import tech.lp2p.proto.IdentifyOuterClass;
import tech.lp2p.quic.Stream;
import tech.lp2p.utils.Utils;

public final class IdentifyHandler implements Handler {

    private final Host host;

    public IdentifyHandler(Host host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) {

        IdentifyOuterClass.Identify identify = IdentifyService.identify(
                host.self(), host.agent(),
                host.protocols(ALPN.libp2p).names(), host.peeraddrs());
        stream.writeOutput(Utils.encode(identify, Protocol.MULTISTREAM_PROTOCOL,
                Protocol.IDENTITY_PROTOCOL), true);
    }

    @Override
    public void data(Stream stream, byte[] data) throws Exception {
        if (data.length > 0) {
            throw new Exception("not expected data received for identify " + new String(data));
        }
    }

    @Override
    public void terminated(Stream stream) {
        // nothing to do here
    }

    @Override
    public void fin(Stream stream) {
        // nothing to do here
    }

}
