package tech.lp2p.cert;

import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

public record JcaContentSignerBuilder(String signatureAlgorithm) {


    public ContentSigner build(PrivateKey privateKey) throws Exception {


        AlgorithmIdentifier sigAlgId = DefaultSignatureAlgorithmIdentifierFinder
                .find(signatureAlgorithm);


        String sigName = DefaultSignatureNameFinder.getAlgorithmName(sigAlgId);
        final Signature sig = Signature.getInstance(sigName);
        sig.initSign(privateKey);

        return new JcaContentSigner(sig, sigAlgId);

    }

    private static class JcaContentSigner implements ContentSigner {
        private final OutputStream stream;
        private final Signature sig;
        private final AlgorithmIdentifier signatureAlgId;

        private JcaContentSigner(Signature sig, AlgorithmIdentifier signatureAlgId) {
            this.sig = sig;
            this.signatureAlgId = signatureAlgId;
            stream = new SignatureUpdatingOutputStream(sig);
        }

        public AlgorithmIdentifier getAlgorithmIdentifier() {
            return signatureAlgId;
        }

        public OutputStream getOutputStream() {
            return stream;
        }

        public byte[] getSignature() throws SignatureException {
            return sig.sign();
        }
    }
}
