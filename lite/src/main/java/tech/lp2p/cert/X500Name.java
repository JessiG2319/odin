package tech.lp2p.cert;

/**
 * The X.500 Name object.
 * <pre>
 *     Name ::= CHOICE {
 *                       RDNSequence }
 *
 *     RDNSequence ::= SEQUENCE OF RelativeDistinguishedName
 *
 *     RelativeDistinguishedName ::= SET SIZE (1..MAX) OF AttributeTypeAndValue
 *
 *     AttributeTypeAndValue ::= SEQUENCE {
 *                                   payloadType  OBJECT IDENTIFIER,
 *                                   value ANY }
 * </pre>
 */
public final class X500Name extends ASN1Object implements ASN1Choice {

    private final RDN[] rdns;
    private final DERSequence rdnSeq;

    public X500Name(String dirName) {
        RDN[] rDNs = BCStyle.getInstance().fromString(dirName);
        this.rdns = rDNs.clone();
        this.rdnSeq = new DERSequence(this.rdns);
    }

    X500Name(RDN[] vals) {
        this.rdns = vals.clone();
        this.rdnSeq = new DERSequence(this.rdns);

    }

    public static X500Name getInstance(Object obj) {
        if (obj instanceof X500Name) {
            return (X500Name) obj;
        }
        return null;
    }


    /**
     * return an array of RDNs in structure order.
     *
     * @return an array of RDN objects.
     */
    public RDN[] getRDNs() {
        return rdns.clone();
    }

    public ASN1Primitive toASN1Primitive() {
        return rdnSeq;
    }


}
