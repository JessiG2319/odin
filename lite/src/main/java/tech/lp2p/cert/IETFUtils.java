package tech.lp2p.cert;


import java.util.Hashtable;
import java.util.Objects;

interface IETFUtils {

    static RDN[] rDNsFromString(String name, X500NameStyle x500Style) {
        X500NameTokenizer nTok = new X500NameTokenizer(name);
        X500NameBuilder builder = new X500NameBuilder(x500Style);

        while (nTok.hasMoreTokens()) {
            String token = nTok.nextToken();
            Objects.requireNonNull(token);

            X500NameTokenizer vTok = new X500NameTokenizer(token, '=');

            String attr = vTok.nextToken();

            if (!vTok.hasMoreTokens()) {
                throw new IllegalArgumentException("badly formatted directory string");
            }

            String value = vTok.nextToken();
            Objects.requireNonNull(attr);
            ASN1ObjectIdentifier oid = x500Style.attrNameToOID(attr.trim());
            Objects.requireNonNull(value);
            builder.addRDN(oid, value.trim());
        }


        return builder.build().getRDNs();
    }

    static ASN1ObjectIdentifier decodeAttrName(String name,
                                               Hashtable<String, ASN1ObjectIdentifier> lookUp) {
        if (Strings.toUpperCase(name).startsWith("OID.")) {
            return new ASN1ObjectIdentifier(name.substring(4));
        } else if (name.charAt(0) >= '0' && name.charAt(0) <= '9') {
            return new ASN1ObjectIdentifier(name);
        }

        ASN1ObjectIdentifier oid = lookUp.get(Strings.toLowerCase(name));
        if (oid == null) {
            throw new IllegalArgumentException("Unknown object id - " + name + " - passed to distinguished name");
        }

        return oid;
    }

}
