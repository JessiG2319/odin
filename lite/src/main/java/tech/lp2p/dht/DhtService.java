package tech.lp2p.dht;


import com.google.protobuf.ByteString;

import org.jetbrains.annotations.NotNull;

import java.net.ConnectException;
import java.util.List;
import java.util.concurrent.TimeoutException;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Protocol;
import tech.lp2p.core.Server;
import tech.lp2p.proto.Dht;
import tech.lp2p.quic.Requester;
import tech.lp2p.utils.Utils;

public interface DhtService {

    @NotNull
    static Dht.Message createFindNodeMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NotNull
    static Dht.Message createGetProvidersMessage(Key key) {
        return Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0).build();
    }

    @NotNull
    static Dht.Message createAddProviderMessage(Server server, Key key) {

        Dht.Message.Builder builder = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.ADD_PROVIDER)
                .setKey(ByteString.copyFrom(key.target()))
                .setClusterLevelRaw(0);

        Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                .setId(ByteString.copyFrom(PeerId.multihash(server.self())));
        for (Peeraddr peeraddr : server.peeraddrs()) {
            peerBuilder.addAddrs(ByteString.copyFrom(peeraddr.encoded()));
        }
        builder.addProviderPeers(peerBuilder.build());

        return builder.build();
    }


    @NotNull
    static Peeraddrs providers(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.readDelimiter())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);
        Peeraddrs peeraddrs = new Peeraddrs();

        Dht.Message pms = Dht.Message.parseFrom(data);

        List<Dht.Message.Peer> list = pms.getProviderPeersList();
        for (Dht.Message.Peer entry : list) {
            if (entry.getAddrsCount() > 0) {
                PeerId peerId = PeerId.parse(entry.getId().toByteArray());
                if (peerId != null) {
                    Peeraddr reduced = Peeraddrs.reduce(peerId, entry.getAddrsList());
                    if (reduced != null) {
                        peeraddrs.add(reduced);
                    }
                }
            }
        }

        return peeraddrs;
    }

    static Peeraddrs findPeeraddrs(Connection connection, Key key) throws Exception {
        if (connection.alpn() != ALPN.libp2p) {
            throw new Exception("wrong ALPN usage");
        }

        Dht.Message message = DhtService.createFindNodeMessage(key);


        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.readDelimiter())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);

        Dht.Message pms = Dht.Message.parseFrom(data);

        Peeraddrs peeraddrs = new Peeraddrs();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {
            if (entry.getAddrsCount() > 0) {
                PeerId peerId = PeerId.parse(entry.getId().toByteArray());
                if (peerId != null) {
                    Peeraddr reduced = Peeraddrs.reduce(peerId, entry.getAddrsList());
                    if (reduced != null) {
                        peeraddrs.add(reduced);
                    }
                }
            }
        }
        return peeraddrs;
    }

    static void message(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException {
        Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.readDelimiter())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_MESSAGE_TIMEOUT);
    }

    @NotNull
    static Dht.Message request(Connection connection, Dht.Message message)
            throws InterruptedException, TimeoutException, ConnectException {

        byte[] data = Requester.createStream((tech.lp2p.quic.Connection) connection,
                        Protocol.DHT_PROTOCOL.readDelimiter())
                .request(Utils.encode(message, Protocol.MULTISTREAM_PROTOCOL,
                        Protocol.DHT_PROTOCOL), Lite.DHT_REQUEST_TIMEOUT);
        try {
            return Dht.Message.parseFrom(data);
        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }

    }

}
