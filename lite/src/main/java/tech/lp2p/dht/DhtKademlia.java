package tech.lp2p.dht;

import org.jetbrains.annotations.NotNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.Lite;
import tech.lp2p.core.ALPN;
import tech.lp2p.core.Acceptor;
import tech.lp2p.core.Host;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Routing;
import tech.lp2p.core.Server;
import tech.lp2p.proto.Dht;
import tech.lp2p.quic.Connection;
import tech.lp2p.quic.ConnectionBuilder;


public final class DhtKademlia implements Routing {
    @NotNull
    private final DhtRoutingTable dhtRoutingTable;
    @NotNull
    private final Host host;
    @NotNull
    private final PeerStore peerStore;
    @NotNull
    private final ReentrantLock lock = new ReentrantLock();

    public DhtKademlia(@NotNull Host host, @NotNull PeerStore peerStore) {
        this.host = host;
        this.peerStore = peerStore;
        this.dhtRoutingTable = new DhtRoutingTable(ConcurrentHashMap.newKeySet());
    }

    @NotNull
    private static List<DhtPeer> evalClosestPeers(@NotNull Dht.Message pms) {

        List<DhtPeer> dhtPeers = new ArrayList<>();

        for (Dht.Message.Peer entry : pms.getCloserPeersList()) {
            if (entry.getAddrsCount() > 0) {
                PeerId peerId = PeerId.parse(entry.getId().toByteArray());
                if (peerId != null) {
                    Peeraddr reduced = Peeraddrs.reduce(peerId, entry.getAddrsList());
                    if (reduced != null) {
                        dhtPeers.add(DhtPeer.create(reduced, true));
                    }
                }
            }
        }
        return dhtPeers;
    }


    private void bootstrap() {
        lock.lock();
        try {
            if (dhtRoutingTable.isEmpty()) {
                Peeraddrs peeraddrs = host.bootstrap();
                for (Peeraddr peeraddr : peeraddrs) {
                    dhtRoutingTable.addPeer(DhtPeer.create(peeraddr, false));
                }
            }
            List<Peeraddr> peeraddrs = peerStore.peeraddrs(Lite.DHT_ALPHA);

            for (Peeraddr peeraddr : peeraddrs) {
                dhtRoutingTable.addPeer(DhtPeer.create(peeraddr, true));
            }
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void findClosestPeers(@NotNull Key key, @NotNull Acceptor acceptor) {

        bootstrap();
        Set<Peeraddr> handled = ConcurrentHashMap.newKeySet();

        Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
        closestPeers(key, findNodeMessage, peers -> {
            for (DhtPeer dhtPeer : peers) {
                if (handled.add(dhtPeer.peeraddr())) {
                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }
                    acceptor.consume(dhtPeer.peeraddr());
                }
            }
        });

    }

    private void closestPeers(@NotNull Key key, @NotNull Dht.Message message,
                              @NotNull QueryClosest closest) {

        runQuery(key, (peer) -> {

            if (Thread.currentThread().isInterrupted()) {
                return Collections.emptyList();
            }

            Dht.Message pms = request(peer, message);

            List<DhtPeer> dhtPeers = evalClosestPeers(pms);

            closest.evaluate(dhtPeers);

            return dhtPeers;
        });

    }


    @Override
    public void providers(@NotNull Key key, @NotNull Acceptor acceptor) {

        bootstrap();

        Dht.Message message = DhtService.createGetProvidersMessage(key);

        Set<PeerId> handled = ConcurrentHashMap.newKeySet();
        runQuery(key, (dhtPeer) -> {

            if (Thread.currentThread().isInterrupted()) {
                return Collections.emptyList();
            }

            Dht.Message pms = request(dhtPeer, message);

            List<Dht.Message.Peer> list = pms.getProviderPeersList();

            for (Dht.Message.Peer entry : list) {
                if (entry.getAddrsCount() > 0) {
                    PeerId peerId = PeerId.parse(entry.getId().toByteArray());
                    if (peerId != null) {
                        Peeraddr reduced = Peeraddrs.reduce(peerId, entry.getAddrsList());
                        if (reduced != null) {
                            if (handled.add(reduced.peerId())) {
                                if (Thread.currentThread().isInterrupted()) {
                                    return Collections.emptyList();
                                }
                                acceptor.consume(reduced);
                            }
                        }
                    }
                }
            }
            if (Thread.currentThread().isInterrupted()) {
                return Collections.emptyList();
            }
            return evalClosestPeers(pms);

        });

    }

    void addToRouting(@NotNull DhtQueryPeer dhtQueryPeer) {

        DhtPeer dhtPeer = dhtQueryPeer.dhtPeer();
        boolean added = dhtRoutingTable.addPeer(dhtPeer);


        // only replaceable peer are adding
        // (the replaceable peers are initial the routing peers)
        if (added && dhtPeer.replaceable()) {
            peerStore.storePeeraddr(dhtPeer.peeraddr());
        }
    }


    public void provideKey(@NotNull Server server,
                           @NotNull Acceptor acceptor,
                           @NotNull Key key) {
        bootstrap();
        Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);
        Dht.Message message = DhtService.createAddProviderMessage(server, key);

        Set<DhtPeer> handled = ConcurrentHashMap.newKeySet();

        closestPeers(key, findNodeMessage, peers -> {

            for (DhtPeer dhtPeer : peers) {
                if (handled.add(dhtPeer)) {

                    if (Thread.currentThread().isInterrupted()) {
                        return;
                    }

                    try (Connection connection = ConnectionBuilder.connect(host,
                            dhtPeer.peeraddr(), Parameters.create(ALPN.libp2p, false,
                                    dhtPeer.replaceable()), host.responder(ALPN.libp2p))) {

                        DhtService.message(connection, message);

                        if (Thread.currentThread().isInterrupted()) {
                            return;
                        }

                        acceptor.consume(connection.remotePeeraddr());

                    }
                }
            }
        });
    }


    @NotNull
    private Dht.Message request(@NotNull DhtPeer dhtPeer,
                                @NotNull Dht.Message message)
            throws InterruptedException, ConnectException, TimeoutException {

        try (Connection connection = ConnectionBuilder.connect(host, dhtPeer.peeraddr(),
                Parameters.create(ALPN.libp2p, false,
                        dhtPeer.replaceable()), host.responder(ALPN.libp2p))) {
            return DhtService.request(connection, message);
        }
    }


    @NotNull
    @Override
    public Peeraddrs findPeeraddrs(@NotNull Key key, int timeout) {

        Set<Peeraddr> result = ConcurrentHashMap.newKeySet();
        bootstrap();
        Dht.Message findNodeMessage = DhtService.createFindNodeMessage(key);

        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            service.execute(() -> runQuery(key, (peer) -> {

                if (Thread.currentThread().isInterrupted()) {
                    service.shutdownNow();
                    return Collections.emptyList();
                }
                Dht.Message pms = request(peer, findNodeMessage);

                List<DhtPeer> dhtPeers = new ArrayList<>();
                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {
                    if (entry.getAddrsCount() > 0) {
                        PeerId peerId = PeerId.parse(entry.getId().toByteArray());
                        if (peerId != null) {
                            if (Arrays.equals(peerId.hash(), key.target())) {
                                Peeraddrs peeraddrs = Peeraddr.create(peerId, entry.getAddrsList());
                                if (!peeraddrs.isEmpty()) {
                                    result.addAll(peeraddrs);
                                    service.shutdownNow();
                                    return Collections.emptyList();
                                }
                            } else {
                                Peeraddr reduced = Peeraddrs.reduce(peerId, entry.getAddrsList());
                                if (reduced != null) {
                                    dhtPeers.add(DhtPeer.create(reduced, true));
                                }
                            }
                        }
                    }
                }
                if (Thread.currentThread().isInterrupted()) {
                    service.shutdownNow();
                    return Collections.emptyList();
                }
                return dhtPeers;
            }));

            service.shutdown();
            if (!service.awaitTermination(timeout, TimeUnit.SECONDS)) {
                service.shutdownNow();
            }
        } catch (InterruptedException interruptedException) {
            service.shutdownNow();
        }

        return new Peeraddrs(result);
    }

    private void runQuery(Key key, QueryFunc queryFn) {
        // pick the K closest peers to the key in our Routing table.
        List<DhtQueryPeer> peers = dhtRoutingTable.nearestPeers(key);
        DhtQuery.runQuery(this, key, peers, queryFn);
    }

    @NotNull
    public Peeraddrs nearestPeers(@NotNull Key key) {

        List<DhtQueryPeer> peers = dhtRoutingTable.nearestPeers(key);
        Peeraddrs peeraddrs = new Peeraddrs();
        for (DhtQueryPeer peer : peers) {
            peeraddrs.add(peer.dhtPeer().peeraddr());
        }
        return peeraddrs;
    }

    void removeFromRouting(DhtQueryPeer peer, boolean removeBootstrap) {
        dhtRoutingTable.removePeer(peer);
        if (removeBootstrap) {
            // remove from bootstrap
            peerStore.removePeeraddr(peer.dhtPeer().peeraddr());
        }
    }


    public interface QueryFunc {
        @NotNull
        List<DhtPeer> query(@NotNull DhtPeer dhtPeer)
                throws InterruptedException, ConnectException, TimeoutException;
    }

    public interface QueryClosest {

        void evaluate(List<DhtPeer> dhtPeers)
                throws InterruptedException, ConnectException, TimeoutException;
    }
}
