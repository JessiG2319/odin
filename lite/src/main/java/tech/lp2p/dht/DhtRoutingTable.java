package tech.lp2p.dht;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import tech.lp2p.Lite;
import tech.lp2p.core.Key;


record DhtRoutingTable(@NotNull Set<DhtPeer> dhtPeers) {

    public List<DhtQueryPeer> nearestPeers(@NotNull Key key) {

        ArrayList<DhtQueryPeer> pds = new ArrayList<>();

        dhtPeers.forEach(peer -> pds.add(DhtQueryPeer.create(peer, key)));

        // Sort by distance to key
        Collections.sort(pds);

        // now get the best result to limit 'Lite.DHT_ALPHA'
        return pds.stream().limit(Lite.DHT_ALPHA).collect(Collectors.toList());

    }

    public boolean addPeer(@NotNull DhtPeer dhtPeer) {

        // peer already exists in the Routing Table.
        if (dhtPeers.contains(dhtPeer)) {
            return false;
        }

        // We have enough space in the table
        if (dhtPeers.size() < Lite.DHT_TABLE_SIZE) {
            return dhtPeers.add(dhtPeer);
        }

        // try to add, but only when not a replaceable is found
        int index = new Random().nextInt(dhtPeers.size());
        Optional<DhtPeer> optional = dhtPeers.stream().skip(index).findFirst();
        if (optional.isPresent()) {
            DhtPeer random = optional.get();
            // let's evict it and add the new peer
            if (random.replaceable()) {
                if (removePeer(random)) {
                    return dhtPeers.add(dhtPeer);
                }
            }
        }
        return false;
    }

    public void removePeer(DhtQueryPeer peer) {
        removePeer(peer.dhtPeer());
    }


    private boolean removePeer(@NotNull DhtPeer dhtPeer) {
        if (dhtPeer.replaceable()) {
            return dhtPeers.remove(dhtPeer);
        }
        return false;
    }

    public boolean isEmpty() {
        return dhtPeers.isEmpty();
    }

}