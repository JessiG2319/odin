package tech.lp2p;


import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import tech.lp2p.core.BlockStore;
import tech.lp2p.core.Cid;
import tech.lp2p.utils.Utils;


public final class Store implements BlockStore {

    private static volatile Store INSTANCE = null;
    private final Lock lock = new ReentrantLock();
    private final File directory;

    private Store() throws IOException {
        String property = System.getProperty("java.io.tmpdir");
        this.directory = new File(property);
        if (!directory.exists()) {
            throw new IOException("Tmpdir does not exists");
        }
    }

    public static Store getInstance() throws IOException {

        if (INSTANCE == null) {
            synchronized (Store.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Store();
                }
            }
        }
        return INSTANCE;
    }


    static void deleteDirectory(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    boolean result = file.delete();
                    TestEnv.error(file.getName() + " deleting " + result);
                }
            }
        }
    }

    public void clear() {
        lock.lock();
        try {
            deleteDirectory(directory);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public boolean hasBlock(@NotNull Cid cid) {
        lock.lock();
        try {
            return getFile(cid).exists();
        } finally {
            lock.unlock();
        }
    }


    @Override
    public byte[] getBlock(@NotNull Cid cid) {
        lock.lock();
        try {
            File file = getFile(cid);
            if (file.exists()) {
                try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(
                        (int) file.length())) {
                    try (FileInputStream fileInputStream = new FileInputStream(file)) {
                        Utils.copy(fileInputStream, outputStream);
                        return outputStream.toByteArray();
                    }
                }
            }
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        } finally {
            lock.unlock();
        }
        return null;
    }

    @Override
    public void storeBlock(@NotNull Cid cid, byte[] data) {
        lock.lock();
        try {
            writeFile(cid, data);
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        } finally {
            lock.unlock();
        }
    }

    File getFile(Cid cid) {
        return new File(directory, cid.toBase32() + ".cid");
    }

    private void writeFile(Cid cid, byte[] data) throws IOException {
        File file = getFile(cid);
        if (!file.exists()) {
            boolean result = file.createNewFile();
            TestEnv.error(file.getName() + " create " + result);
        }
        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(data)) {
                Utils.copy(inputStream, fileOutputStream);
            }
        }
    }

    @Override
    public void deleteBlock(@NotNull Cid cid) {
        lock.lock();
        try {
            File file = getFile(cid);
            if (file.exists()) {
                boolean result = file.delete();
                TestEnv.error(file.getName() + " deleting " + result);
            }
        } finally {
            lock.unlock();
        }
    }
}
