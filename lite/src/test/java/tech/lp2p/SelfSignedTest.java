package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.cert.X509Certificate;

import tech.lp2p.core.Certificate;
import tech.lp2p.core.PeerId;


public class SelfSignedTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void certificateTest() throws Exception {

        TestEnv.error(Certificate.integersToString(Certificate.PREFIXED_EXTENSION_ID));


        assertNotNull(Certificate.getLiteExtension());

        Lite lite = TestEnv.getTestInstance();

        X509Certificate cert = Certificate.createCertificate(lite.keys()).x509Certificate();
        assertNotNull(cert);

        PeerId peerId = Certificate.extractPeerId(cert);
        assertNotNull(peerId);
        assertEquals(peerId, lite.self());

    }

}
