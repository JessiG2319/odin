package tech.lp2p;


import tech.lp2p.core.BlockStore;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.PeerStore;
import tech.lp2p.core.Session;
import tech.lp2p.lite.LiteHost;
import tech.lp2p.lite.LiteSession;

public final class Dummy {
    private final LiteHost host;
    private final BlockStore blockStore = new TestEnv.DummyBlockStore();
    private final PeerStore peerStore = new TestEnv.DummyPeerStore();

    public Dummy() throws Exception {
        this.host = new LiteHost(Lite.generateKeys(), TestEnv.BOOTSTRAP, TestEnv.AGENT);
    }

    public PeerId self() {
        return host.self();
    }

    public Session createSession() {
        return new LiteSession(blockStore, peerStore, host);
    }


}
