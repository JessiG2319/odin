package tech.lp2p;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;

public class ProvideTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testProvideCidKey() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        byte[] data = TestEnv.getRandomBytes(100);
        Raw cid = Lite.storeData(session, data);
        assertNotNull(cid);
        assertNull(cid.name());

        long start = System.currentTimeMillis();

        Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
        Key key = cid.cid().createKey();

        Utils.runnable(() -> Lite.provideKey(server, key, providers::add), 60);


        TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                " number of providers " + providers.size());


        Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();

        Utils.runnable(() -> Lite.providers(session, key, peeraddrs::add), 60);


        TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                " number of peeraddrs " + peeraddrs.size());
        assertTrue(peeraddrs.isEmpty()); // random cid, should not be found


    }


    @Test
    public void testProvidePeerIdKey() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        PeerId self = lite.self();

        long start = System.currentTimeMillis();

        Set<Peeraddr> providers = ConcurrentHashMap.newKeySet();
        Key key = self.createKey();

        Utils.runnable(() -> Lite.provideKey(server, key, providers::add), 60);


        TestEnv.error("Time provide " + (System.currentTimeMillis() - start) +
                " number of providers " + providers.size());

        Set<Peeraddr> peeraddrs = ConcurrentHashMap.newKeySet();

        Utils.runnable(() -> Lite.providers(session, key, peeraddrs::add), 60);

        if (TestEnv.hasNetwork()) {
            assertFalse(peeraddrs.isEmpty());
        }

    }
}
