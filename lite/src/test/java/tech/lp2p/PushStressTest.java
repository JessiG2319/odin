package tech.lp2p;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class PushStressTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void pushIterations() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        Dummy dummy = new Dummy();

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session dummySession = dummy.createSession();

        Parameters parameters = Lite.createParameters(false);


        Cid cid = Lite.rawCid("moin");
        Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, cid);

        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            for (int i = 0; i < TestEnv.ITERATIONS; i++) {
                Lite.pushEnvelope(connection, data);
            }
        }
    }
}
