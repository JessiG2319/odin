package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Limit;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.ident.IdentifyService;
import tech.lp2p.quic.Requester;
import tech.lp2p.utils.Utils;

public class RelayTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test(expected = Exception.class)
    public void permissionDeniedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        Lite.hopReserve(server, relayPeeraddr); // exception expected
        fail();

    }


    @Test(expected = Exception.class)
    public void permissionDeniedConnection() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        Session session = lite.createSession(
                new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore());
        Parameters parameters = Lite.createParameters();
        Lite.hopConnect(session, relayPeeraddr, lite.self(), parameters); // exception expected
        fail();


    }

    @Test
    public void positiveReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {
            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // check if server has connection
            List<Connection> connections = server.connections(aliceServer.self());
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            assertTrue(connection.hasAttribute(Requester.RELAYED));

            Lite bob = new Lite(Lite.createSettings(Lite.generateKeys(),
                    new Peeraddrs(), TestEnv.AGENT));

            Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore());

            Parameters parameters = Parameters.create(ALPN.libp2p);
            try (Connection bobToAlice = Lite.hopConnect(bobSession, relayPeeraddr,
                    alice.self(), parameters)) {
                assertNotNull(bobToAlice);

                Peeraddr aliceRemote = bobToAlice.remotePeeraddr();
                assertNotNull(aliceRemote);
                assertEquals(aliceRemote.port(), alicePort);

                Identify identify = IdentifyService.identify(bobToAlice);
                assertNotNull(identify);
                assertEquals(identify.peerId(), alice.self());
                assertEquals(identify.peerId(), bobToAlice.remotePeerId());

                assertTrue(bobToAlice.isConnected());
            }


            reservations = Lite.reservations(aliceServer);
            assertFalse(reservations.isEmpty()); // should not be empty


            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty
        } finally {
            aliceServer.shutdown();
        }


    }


    @Test
    public void bobDeniedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));


        List<PeerId> aliceGated = new ArrayList<>();
        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                aliceGated::contains,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // check if server has connection
            List<Connection> connections = server.connections(aliceServer.self());
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            assertTrue(connection.hasAttribute(Requester.RELAYED));

            Lite bob = new Lite(Lite.createSettings(Lite.generateKeys(),
                    new Peeraddrs(), TestEnv.AGENT));

            aliceGated.add(bob.self()); // bob is now gated for alice

            Session bobSession = bob.createSession(
                    new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore());

            Parameters parameters = Parameters.create(ALPN.libp2p);

            boolean failed = false;
            try {
                // this will fail
                Lite.hopConnect(bobSession, relayPeeraddr, alice.self(), parameters);
            } catch (Throwable expected) {
                failed = true;
            }
            assertTrue(failed);


            reservations = Lite.reservations(aliceServer);
            assertFalse(reservations.isEmpty()); // should not be empty


            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty
        } finally {
            aliceServer.shutdown();
        }

    }


    @Test
    public void reservationRefused() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);

            Thread.sleep(3000);
            boolean failed = false;
            try {
                Lite.hopReserve(aliceServer, relayPeeraddr);
            } catch (Throwable expected) {
                failed = true;
            }
            assertTrue(failed);

            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), reservation);

            Lite.closeReservation(aliceServer, reservation);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

        } finally {
            aliceServer.shutdown();
        }

    }


    @Test
    public void refreshReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);

            Thread.sleep(1000);

            Reservation refreshed = Lite.refreshReservation(aliceServer, reservation);
            assertNotEquals(reservation, refreshed);
            assertTrue(refreshed.expireInMinutes() > reservation.expireInMinutes());

            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);
            assertEquals(reservations.iterator().next(), refreshed);

            Lite.closeReservation(aliceServer, refreshed);
            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 0);

        } finally {
            aliceServer.shutdown();
        }

    }


    @Test
    public void reservationLost() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);

        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            // get the connection with the peerId from alice from the lite server and close it
            PeerId alicePeerId = alice.self();
            assertNotNull(alicePeerId);
            List<Connection> connections = server.connections(alicePeerId);
            assertNotNull(connections);
            assertEquals(connections.size(), 1);
            Connection connection = connections.get(0);
            assertNotNull(connection);
            connection.close();

            Thread.sleep(3000); // takes time to be recognized

            // now check that the reservation is not returned by the aliceServer
            reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 0);

        } finally {
            aliceServer.shutdown();
        }
    }


    @Test
    public void twoReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        int bobPort = Utils.nextFreePort();
        Lite bob = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));
        serverSettings = Lite.createServerSettings(bobPort);
        Server bobServer = bob.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to bobServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from bobServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {
            Reservation reservationAlice = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservationAlice);
            assertEquals(relayPeeraddr, reservationAlice.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            Reservation reservationBob = Lite.hopReserve(bobServer, relayPeeraddr);
            assertNotNull(reservationBob);
            assertEquals(relayPeeraddr, reservationBob.peeraddr());

            reservations = Lite.reservations(bobServer);
            assertEquals(reservations.size(), 1);

            // now closing
            Lite.closeReservation(aliceServer, reservationAlice);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty


            Lite.closeReservation(bobServer, reservationBob);
            reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

        } finally {
            aliceServer.shutdown();
        }
    }


    @Test(expected = ConnectException.class)
    public void limitedReservation() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        int port = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(port,
                new Limit(10, 10)); // limit should fail for reservation

        Server server = lite.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to server "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from server "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        assertNotNull(server);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettingsAlice = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettingsAlice, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {

            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);


            Lite bob = new Lite(Lite.createSettings(Lite.generateKeys(),
                    new Peeraddrs(), TestEnv.AGENT));

            try {

                Session bobSession = bob.createSession(
                        new TestEnv.DummyBlockStore(), new TestEnv.DummyPeerStore());
                Parameters parameters = Parameters.create(ALPN.libp2p);

                // this should fail
                Lite.hopConnect(bobSession, relayPeeraddr, alice.self(), parameters);
                fail(); // should never reach this point
            } finally {

                // reservation is still valid
                reservations = Lite.reservations(aliceServer);
                assertFalse(reservations.isEmpty()); // should not be empty

                Lite.closeReservation(aliceServer, reservation);
                reservations = Lite.reservations(aliceServer);
                assertTrue(reservations.isEmpty()); // should be empty
            }

        } finally {
            server.shutdown();
            aliceServer.shutdown();
        }
    }

    @Test(expected = ConnectException.class)
    public void serverShutdown() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        int port = Utils.nextFreePort();
        Server.Settings serverSettings = Lite.createServerSettings(port,
                new Limit(10, 10)); // limit should fail for reservation

        Server server = lite.startServer(serverSettings, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to server "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from server "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        assertNotNull(server);

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr relayPeeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(relayPeeraddr);


        int alicePort = Utils.nextFreePort();
        Server.Settings serverSettingsAlice = Lite.createServerSettings(alicePort);
        Lite alice = new Lite(Lite.createSettings(Lite.generateKeys(),
                new Peeraddrs(), TestEnv.AGENT));

        Server aliceServer = alice.startServer(serverSettingsAlice, new TestEnv.DummyBlockStore(),
                new TestEnv.DummyPeerStore(),
                peeraddrGain -> TestEnv.error("Connect to aliceServer "
                        + peeraddrGain.toString()),
                peeraddrLost -> TestEnv.error("Disconnect from aliceServer "
                        + peeraddrLost.toString()),
                reservationGain -> TestEnv.error("Reservation gain "
                        + reservationGain.toString()),
                reservationLost -> TestEnv.error("Reservation lost "
                        + reservationLost.toString()),
                peerId -> false,
                payload -> null,
                envelope -> {
                });

        try {
            Reservation reservation = Lite.hopReserve(aliceServer, relayPeeraddr);
            assertNotNull(reservation);
            assertEquals(relayPeeraddr, reservation.peeraddr());

            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertEquals(reservations.size(), 1);

            server.shutdown(); // server shutdown

            // this is only to make the test run (the shutdown is not detected by the
            // reservation connection (because shutdown does not send anything)
            // but an refresh of the reservation will help
            Lite.refreshReservation(aliceServer, reservation);
            fail(); // should never reach this point

        } finally {
            Set<Reservation> reservations = Lite.reservations(aliceServer);
            assertTrue(reservations.isEmpty()); // should be empty

            aliceServer.shutdown();
        }
    }

}
