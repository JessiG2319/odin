package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import tech.lp2p.core.Key;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.dht.DhtPeer;
import tech.lp2p.dht.DhtPeerState;
import tech.lp2p.dht.DhtQueryPeer;
import tech.lp2p.dht.DhtQueryPeers;

public class DhtQueryPeersTest {

    @Test
    public void saturationModeNotReplaced() throws UnknownHostException {
        DhtQueryPeers dhtQueryPeers = new DhtQueryPeers(2);
        assertNotNull(dhtQueryPeers);

        Key key = Key.convertKey("Moin".getBytes());
        dhtQueryPeers.add(random(key));
        dhtQueryPeers.add(random(key));
        assertEquals(dhtQueryPeers.size(), 2);

        dhtQueryPeers.add(perfect(key));
        assertTrue(dhtQueryPeers.size() >= 2); // can be three
    }

    @Test
    public void saturationModeReplaced() throws UnknownHostException {
        DhtQueryPeers dhtQueryPeers = new DhtQueryPeers(2);
        assertNotNull(dhtQueryPeers);

        Key key = Key.convertKey("Moin".getBytes());
        DhtQueryPeer a = random(key);
        dhtQueryPeers.add(a);
        DhtQueryPeer b = random(key);
        dhtQueryPeers.add(b);
        assertEquals(dhtQueryPeers.size(), 2);
        a.setState(DhtPeerState.PeerQueried);
        b.setState(DhtPeerState.PeerQueried);

        dhtQueryPeers.add(perfect(key));
        assertEquals(dhtQueryPeers.size(), 2); // can be three
    }

    @Test
    public void random() throws UnknownHostException {
        DhtQueryPeers dhtQueryPeers = new DhtQueryPeers(2);
        assertNotNull(dhtQueryPeers);

        Key key = Key.convertKey("Moin".getBytes());
        for (int i = 0; i < 20; i++) {
            DhtQueryPeer a = random(key);
            dhtQueryPeers.add(a);
            if (i > 10) {
                a.setState(DhtPeerState.PeerUnreachable);
            } else {
                a.setState(DhtPeerState.PeerQueried);
            }
        }

        dhtQueryPeers.add(perfect(key));
        assertEquals(dhtQueryPeers.size(), 2);
    }

    private DhtQueryPeer random(Key key) throws UnknownHostException {
        PeerId random = TestEnv.random();
        Peeraddr peeraddr = Lite.createPeeraddr(random,
                InetAddress.getByName("139.178.68.146"), 4001);
        DhtPeer dhtPeer = DhtPeer.create(peeraddr, false);
        return DhtQueryPeer.create(dhtPeer, key);
    }

    private DhtQueryPeer perfect(Key key) throws UnknownHostException {
        PeerId random = PeerId.toPeerId(key.hash().bytes());
        Peeraddr peeraddr = Lite.createPeeraddr(random,
                InetAddress.getByName("139.178.68.146"), 4001);
        DhtPeer dhtPeer = DhtPeer.create(peeraddr, false);
        return DhtQueryPeer.create(dhtPeer, key);
    }
}
