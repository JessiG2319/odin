package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigInteger;
import java.net.InetAddress;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Identify;
import tech.lp2p.core.Key;
import tech.lp2p.core.Keys;
import tech.lp2p.core.Network;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;


public class UtilsTest {

    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testAddress() throws Exception {
        PeerId peerId = TestEnv.random();
        Peeraddr peeraddr = Lite.createPeeraddr(peerId.toBase58(),
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001);
        Assert.assertNotNull(peeraddr);

        Peeraddr cmp = Peeraddr.create(peerId, peeraddr.encoded());
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(peeraddr.peerId(), cmp.peerId());
        TestCase.assertEquals(peeraddr, cmp);
    }

    @Test
    public void testHash() {
        Hash hash = Hash.createHash("Min".getBytes());
        byte[] data = Hash.toArray(hash);
        assertArrayEquals(data, hash.bytes());
        Hash cmp = Hash.toHash(data);
        assertEquals(hash, cmp);
    }


    @Test
    public void peerIdRandom() {
        PeerId peerId = TestEnv.random();
        byte[] bytes = PeerId.toArray(peerId);
        TestCase.assertEquals(PeerId.toPeerId(bytes), peerId);
    }


    @Test
    public void network() throws Exception {

        int port = 4001;

        Lite lite = TestEnv.getTestInstance();

        List<InetAddress> siteLocalAddresses = Network.siteLocalAddresses();
        Assert.assertNotNull(siteLocalAddresses);
        TestEnv.error(siteLocalAddresses.toString());

        Peeraddr ma = Peeraddr.loopbackPeeraddr(lite.self(), port);
        Assert.assertNotNull(ma);
        TestEnv.error(ma.toString());


    }

    @Test
    public void distance() throws Exception {

        Lite lite = TestEnv.getTestInstance();

        PeerId peerId = lite.self();


        Key a = peerId.createKey();
        Key b = peerId.createKey();


        BigInteger dist = Key.distance(a, b);
        TestCase.assertEquals(dist.longValue(), 0L);


        PeerId randrom = TestEnv.random();

        Key r1 = randrom.createKey();

        BigInteger distCmp = Key.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        Cid cid = Lite.rawCid("time");
        Assert.assertNotNull(cid);

    }


    @Test
    public void tinkEd25519() throws Exception {
        Keys keys = Lite.generateKeys();

        PeerId peerId = keys.peerId();

        byte[] msg = "moin moin".getBytes();
        byte[] signature = keys.sign(msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keys.privateKey());
        Assert.assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keys.peerId().hash());
        Assert.assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKey = decoder.decode(privateKeyAsString);
        Assert.assertNotNull(privateKey);
        byte[] publicKey = decoder.decode(publicKeyAsString);

        PeerId peerIdCmp = PeerId.create(publicKey);

        TestCase.assertEquals(peerId, peerIdCmp);

        peerIdCmp.verify(msg, signature);

    }

    @Test
    public void cidConversion() {

        Cid cid = Lite.rawCid("moin welt");
        Assert.assertNotNull(cid);
        Assert.assertNotNull(cid.hash());
        assertNotNull(cid.toBase32());

        byte[] data = Cid.toArray(cid);
        Cid cmp = Cid.toCid(data);
        Assert.assertNotNull(cmp);
        TestCase.assertEquals(cmp, cid);
        assertArrayEquals(cmp.hash(), cid.hash());
    }

    @Test
    public void threadModel() throws InterruptedException {
        AtomicBoolean finishedService = new AtomicBoolean(false);
        ExecutorService controller = Executors.newFixedThreadPool(2);
        controller.execute(() -> Utils.runnable(() -> Utils.runnable(() -> {
            try {
                Thread.sleep(Integer.MAX_VALUE);
            } catch (InterruptedException interruptedException) {
                finishedService.set(true);
            }
        }, Integer.MAX_VALUE), Integer.MAX_VALUE));

        // this closes is for interruption
        controller.execute(() -> {
            try {
                Thread.sleep(3000);
                controller.shutdownNow();
            } catch (Throwable throwable) {
                TestEnv.error(throwable);
            }
        });

        controller.shutdown();
        AtomicBoolean finished = new AtomicBoolean(false);
        try {
            finished.set(controller.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS));
        } catch (Throwable throwable) {
            TestEnv.error(throwable);
        }
        assertTrue(finished.get());
        Thread.sleep(5000);
        assertTrue(finishedService.get());
    }

    @Test
    public void cidsDecoding() throws Exception {
        Cid cid = Lite.rawCid("hello");
        assertNotNull(cid);
        Cid cmp = Lite.decodeCid(cid.toBase32());
        assertNotNull(cmp);
        assertEquals(cid, cmp);
    }

    @Test
    public void peerIdsDecoding() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        PeerId peerId = Lite.decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N");
        assertNotNull(peerId);

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = Lite.decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA");
        assertNotNull(peerId);

        peerId = Lite.decodePeerId(lite.self().toBase36());
        assertNotNull(peerId);
        assertEquals(peerId, lite.self());

        peerId = Lite.decodePeerId(lite.self().toBase58());
        assertNotNull(peerId);
        assertEquals(peerId, lite.self());
    }

    @Test
    public void keys() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Keys keys = lite.keys();
        assertNotNull(keys.privateKey());
        assertNotNull(keys.peerId());
    }

    @Test
    public void ipv6Test() throws Exception {
        assertNotNull(Lite.createPeeraddr("12D3KooWQ6SJ5A3uX5WjxCNbEbdAu8ufKJ3TmcjReTLSGaFk4HDU",
                "2804:d41:432f:3f00:ccbd:8e0d:a023:376b", 4001));
    }

    @Test
    public void listenAddresses() throws Exception {
        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Peeraddr[] result = server.identify().peeraddrs();
        assertNotNull(result);
        for (Peeraddr ma : result) {
            TestEnv.error("Listen Address : " + ma.toString());
        }


        Identify identify = server.identify();
        Objects.requireNonNull(identify);

        assertNotNull(identify.peerId());
        assertTrue(identify.hasRelayHop());
        assertEquals(identify.agent(), TestEnv.AGENT);
        assertEquals(identify.peerId(), lite.self());

        String[] protocols = identify.protocols();
        for (String protocol : protocols) {
            assertNotNull(protocol);
        }

        TestEnv.error(identify.toString());
    }


    @Test
    public void streamTest() throws Exception {
        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(
                TestEnv.getBlockStore(), TestEnv.PEERS);
        String test = "Moin";
        Raw cid = Lite.storeText(session, test);
        assertNotNull(cid);
        byte[] bytes = Lite.fetchData(session, cid.cid());
        assertNotNull(bytes);
        assertEquals(test, new String(bytes));


        try {
            Cid fault = Lite.decodeCid(lite.self().toBase58());
            Lite.fetchData(session, fault);
            fail();
        } catch (Exception ignore) {
            // ok
        }


    }

    @Test
    public void textProgressTest() throws Exception {
        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(
                TestEnv.getBlockStore(), TestEnv.PEERS);
        String test = "Moin bla bla";
        Raw cid = Lite.storeText(session, test);
        assertNotNull(cid);

        byte[] bytes = Lite.fetchData(session, cid.cid());
        assertNotNull(bytes);
        assertEquals(test, new String(bytes));

        String text = Lite.fetchText(session, cid.cid());
        assertNotNull(text);
        assertEquals(test, text);


    }


    @Test
    public void addCat() throws Exception {
        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(
                TestEnv.getBlockStore(), TestEnv.PEERS);
        byte[] content = TestEnv.getRandomBytes(400000);

        Fid cid = TestEnv.createContent(session, "random.bin", content);

        byte[] fileContents = Lite.fetchData(session, cid.cid());
        assertNotNull(fileContents);
        assertEquals(content.length, fileContents.length);
        assertEquals(new String(content), new String(fileContents));

        Lite.removeBlocks(session, cid.cid());

    }

    @Test
    public void rawCid() throws Exception {

        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(
                TestEnv.getBlockStore(), TestEnv.PEERS);
        Raw cid = Lite.storeText(session, "hallo");
        assertNotNull(cid);

    }

    @Test
    public void peerStoreTest() throws Exception {

        int port = 4001;

        Lite lite = TestEnv.getTestInstance();
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), port);
        Assert.assertNotNull(peeraddr);
        byte[] data = peeraddr.encoded();
        Assert.assertNotNull(data);
        Peeraddr cmp = Peeraddr.create(lite.self(), data);
        Assert.assertNotNull(cmp);
        assertEquals(peeraddr, cmp);
    }
}
