package tech.lp2p;


import static junit.framework.TestCase.assertFalse;

import com.google.protobuf.ByteString;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Hash;
import tech.lp2p.core.Key;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Session;
import tech.lp2p.proto.Merkledag;
import tech.lp2p.proto.Unixfs;
import tech.lp2p.utils.Utils;

public class ProvidesTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void providersEmptyDirectory() throws Exception {

        Lite lite = TestEnv.getTestInstance();


        // just for testing creates an empty directory
        byte[] unixData = Unixfs.Data.newBuilder()
                .setType(Unixfs.Data.DataType.Directory)
                .build().toByteArray();
        Merkledag.PBNode.Builder pbn = Merkledag.PBNode.newBuilder();
        pbn.setData(ByteString.copyFrom(unixData));
        byte[] bytes = pbn.build().toByteArray();
        Cid cid = Cid.createCid(Hash.createHash(bytes));

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        long time = System.currentTimeMillis();

        Set<Peeraddr> provs = ConcurrentHashMap.newKeySet();
        Key key = Key.convertKey(cid.multihash());

        Utils.runnable(() -> Lite.providers(session, key, provs::add), 60);

        if (TestEnv.hasNetwork()) {
            assertFalse(provs.isEmpty());
        }

        for (Peeraddr prov : provs) {
            TestEnv.error("Provider " + prov);
        }
        TestEnv.error("Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");
    }

}
