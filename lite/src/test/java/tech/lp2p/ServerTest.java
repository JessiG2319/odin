package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Network;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Raw;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;


public class ServerTest {

    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void fetchDataTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Dummy dummy = new Dummy();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

        Fid cid = TestEnv.createContent(session, "random.bin", input);
        assertNotNull(cid);


        Session dummySession = dummy.createSession();

        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            byte[] output = Lite.fetchData(connection, cid.cid(), new TestEnv.DummyProgress());
            assertArrayEquals(input, output);

        }


    }


    @Test
    public void reachableTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        assertNotNull(lite);
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Peeraddrs peeraddrs = server.peeraddrs();
        assertNotNull(peeraddrs);
        boolean reachable = false;
        for (Peeraddr peeraddr : peeraddrs) {
            InetAddress inetAddress = InetAddress.getByAddress(peeraddr.address());

            TestEnv.error("Well known ipv4 translatable " +
                    Network.isWellKnownIPv4Translatable(inetAddress));

            boolean reach = inetAddress.isReachable(50);
            if (reach) {
                TestEnv.error("Reachable " + inetAddress.getHostAddress());
                reachable = true;
            } else {
                TestEnv.error("Not reachable " + inetAddress.getHostAddress());
                TestEnv.error("Site Local " + inetAddress.isSiteLocalAddress());
                TestEnv.error("Link Local " + inetAddress.isLinkLocalAddress());
                TestEnv.error("Any Local " + inetAddress.isAnyLocalAddress());
            }
        }
        assertTrue(reachable);
    }


    @Test
    public void serverTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());
        assertNotNull(peeraddr);

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        String text = "Hallo das ist ein Test";
        Raw cid = Lite.storeText(session, text);
        assertNotNull(cid);

        Dummy dummy = new Dummy();
        Session dummySession = dummy.createSession();

        PeerId host = lite.self();
        assertNotNull(host);

        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);


            // simple push test
            Cid nsToCid = Lite.rawCid("moin moin");
            Envelope data =
                    Lite.createEnvelope(dummySession, TestEnv.RAW, nsToCid);

            Lite.pushEnvelope(connection, data);
            Thread.sleep(1000);
            assertArrayEquals(TestEnv.PUSHED.get().cid().hash(), nsToCid.hash());

            String cmpText = Lite.fetchText(connection, cid.cid(), new TestEnv.DummyProgress());
            assertEquals(text, cmpText);


        }


    }


    @Test
    public void multipleConns() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        byte[] input = TestEnv.getRandomBytes(50000);

        Raw cid = Lite.storeData(session, input);
        assertNotNull(cid);

        ExecutorService executors = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        AtomicInteger finished = new AtomicInteger();
        int instances = 100;
        int timeSeconds = 200;
        for (int i = 0; i < instances; i++) {

            executors.execute(() -> {

                try {
                    Dummy dummy = new Dummy();
                    Session dummySession = dummy.createSession();

                    PeerId serverPeer = lite.self();
                    assertNotNull(serverPeer);
                    PeerId client = dummy.self();
                    assertNotNull(client);

                    Parameters parameters = Lite.createParameters();
                    try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
                        assertNotNull(connection);

                        byte[] output = Lite.fetchData(connection, cid.cid(),
                                new TestEnv.DummyProgress());
                        assertArrayEquals(input, output);
                    }

                    finished.incrementAndGet();

                } catch (Throwable throwable) {
                    TestEnv.error(throwable);
                    fail();
                }
            });
        }
        executors.shutdown();

        assertTrue(executors.awaitTermination(timeSeconds, TimeUnit.SECONDS));
        assertEquals(finished.get(), instances);
    }


}