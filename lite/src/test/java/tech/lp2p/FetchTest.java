package tech.lp2p;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class FetchTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testClientServerDownload() throws Exception {

        Dummy dummy = new Dummy();

        Lite lite = TestEnv.getTestInstance();

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        // create dummy session
        int maxNumberBytes = 10 * 1000 * 1000; // 10 MB

        Fid cid;
        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        AtomicInteger counter = new AtomicInteger(0);
        //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
        cid = Lite.storeInputStream(session, "random.bin", new InputStream() {
            @Override
            public int read() {
                int count = counter.incrementAndGet();
                if (count > maxNumberBytes) {
                    return -1;
                }
                return 99;
            }
        });


        assertNotNull(cid);
        long start = System.currentTimeMillis();

        PeerId host = lite.self();
        TestCase.assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());


        File file = TestEnv.createCacheFile();


        Session dummySession = dummy.createSession();
        Parameters parameters = Lite.createParameters(false);

        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Lite.fetchToFile(connection, file, cid.cid(), new TestEnv.DummyProgress());
        }


        assertEquals(file.length(), maxNumberBytes);

        long end = System.currentTimeMillis();
        TestEnv.error("Time for downloading " + (end - start) / 1000 +
                "[s]" + " " + file.length() / 1000000 + " [MB]");
        file.deleteOnExit();

    }

}
