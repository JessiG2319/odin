package tech.lp2p;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Fid;
import tech.lp2p.core.Session;
import tech.lp2p.utils.Utils;

public class PerformanceTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void smallContentReadWrite() throws Exception {

        int packetSize = 1000;
        long maxData = 100;

        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        File inputFile = TestEnv.createCacheFile();
        try (OutputStream outputStream = new FileOutputStream(inputFile)) {
            for (int i = 0; i < maxData; i++) {
                byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                outputStream.write(randomBytes);
            }
        }
        AtomicInteger counter = new AtomicInteger(0);
        try (OutputStream outputStream = new ByteArrayOutputStream()) {
            try (InputStream inputStream = new FileInputStream(inputFile)) {
                Utils.copy(inputStream, outputStream, counter::set, inputFile.length());

                assertEquals(100, counter.get());
            }
        }
        long size = inputFile.length();


        TestEnv.error("Bytes : " + inputFile.length() / 1000 + "[kb]");
        long now = System.currentTimeMillis();
        Fid cid = Lite.storeFile(session, inputFile);
        assertNotNull(cid);
        TestEnv.error("Add : " + cid +
                " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");

        File file = TestEnv.createCacheFile();
        file.deleteOnExit();
        assertTrue(file.exists());
        assertTrue(file.delete());


        byte[] data = Lite.fetchData(session, cid.cid());
        Objects.requireNonNull(data);


        assertEquals(data.length, size);

        File temp = TestEnv.createCacheFile();
        Lite.fetchToFile(session, temp, cid.cid());

        assertEquals(temp.length(), size);

        assertTrue(temp.delete());
        assertTrue(inputFile.delete());


        Lite.removeBlocks(session, cid.cid());


    }


    @Test
    public void compareFiles() throws Exception {

        int packetSize = 10000;
        long maxData = 5000;


        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        File inputFile = TestEnv.createCacheFile();
        try (OutputStream outputStream = new FileOutputStream(inputFile)) {
            for (int i = 0; i < maxData; i++) {
                byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                outputStream.write(randomBytes);
            }
        }
        long size = inputFile.length();

        Fid fid = Lite.storeFile(session, inputFile);
        assertNotNull(fid);
        File file = TestEnv.createCacheFile();
        Lite.fetchToFile(session, file, fid.cid());

        assertEquals(file.length(), size);

        assertTrue(file.delete());
        assertTrue(inputFile.delete());


        Lite.removeBlocks(session, fid.cid());


    }

    @Test
    public void performanceContentReadWrite() throws Exception {


        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        int packetSize = 10000;
        long maxData = 25000;


        File inputFile = TestEnv.createCacheFile();

        try (OutputStream outputStream = new FileOutputStream(inputFile)) {
            for (int i = 0; i < maxData; i++) {
                byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                outputStream.write(randomBytes);
            }
        }

        long size = inputFile.length();
        assertEquals(packetSize * maxData, size);


        Fid fid = Lite.storeFile(session, inputFile);
        assertNotNull(fid);


        File temp = TestEnv.createCacheFile();

        Lite.fetchToFile(session, temp, fid.cid());
        assertEquals(temp.length(), size);


        Fid fid2 = Lite.storeFile(session, inputFile);
        assertNotNull(fid2);

        assertEquals(fid, fid2);


        File outputFile1 = TestEnv.createCacheFile();
        Lite.fetchToFile(session, outputFile1, fid.cid());


        File outputFile2 = TestEnv.createCacheFile();
        Lite.fetchToFile(session, outputFile2, fid.cid());


        assertEquals(outputFile1.length(), size);
        assertEquals(outputFile2.length(), size);
        assertTrue(outputFile2.delete());
        assertTrue(outputFile1.delete());
        assertTrue(inputFile.delete());


        Lite.removeBlocks(session, fid.cid());


    }

    @Test
    public void contentWrite() throws Exception {


        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        int packetSize = 1000;
        long maxData = 1000;


        File inputFile = TestEnv.createCacheFile();
        try (OutputStream outputStream = new FileOutputStream(inputFile)) {
            for (int i = 0; i < maxData; i++) {
                byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                outputStream.write(randomBytes);
            }
        }

        long size = inputFile.length();
        assertEquals(packetSize * maxData, size);

        Fid fid = Lite.storeFile(session, inputFile);
        assertNotNull(fid);

    }


    @Test
    public void storeInputStream() throws Exception {

        int packetSize = 1000;
        long maxData = 100;

        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        File inputFile = TestEnv.createCacheFile();
        try (OutputStream outputStream = new FileOutputStream(inputFile)) {
            for (int i = 0; i < maxData; i++) {
                byte[] randomBytes = TestEnv.getRandomBytes(packetSize);
                outputStream.write(randomBytes);
            }
        }
        long size = inputFile.length();

        AtomicInteger counter = new AtomicInteger(0);
        try (InputStream inputStream = new FileInputStream(inputFile)) {
            Fid fid = Lite.storeInputStream(session, "dummy.bin", inputStream,
                    counter::set, size);
            assertNotNull(fid);

            assertEquals(counter.get(), 100);

            File temp = TestEnv.createCacheFile();
            Lite.fetchToFile(session, temp, fid.cid());

            assertEquals(temp.length(), inputFile.length());

            assertTrue(temp.delete());
        }
        assertTrue(inputFile.delete());
    }

}
