package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class FetchStressDataTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void stressFetchData() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        long now = System.currentTimeMillis();
        long length = 50000000; // 50 MB
        Fid fid;
        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        AtomicInteger write = new AtomicInteger(0);
        fid = Lite.storeInputStream(session, "test.bin", new InputStream() {
            @Override
            public int read() {
                int count = write.incrementAndGet();
                if (count > length) {
                    return -1;
                }
                return 0; // only zeros, just test the fetch performance
            }
        });


        assertNotNull(fid);

        TestEnv.error("Store Input Stream : " + fid +
                " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
        now = System.currentTimeMillis();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());


        Dummy dummy = new Dummy();
        Session dummySession = dummy.createSession();
        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            AtomicInteger read = new AtomicInteger(0);
            Lite.fetchToOutputStream(connection, new OutputStream() {
                        @Override
                        public void write(int b) {
                            read.incrementAndGet();
                        }
                    },
                    fid.cid(), new TestEnv.DummyProgress());

            TestCase.assertEquals(read.get(), length);
        }

        TestEnv.error("Read Data : " + fid +
                " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");

    }

}
