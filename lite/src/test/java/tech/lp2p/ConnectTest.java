package tech.lp2p;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Session;


public class ConnectTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void findPeer() throws Exception {
        Lite lite = TestEnv.getTestInstance();


        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        List<PeerId> bootstrap = Arrays.asList(
                Lite.decodePeerId("12D3KooWHXX3heMQfq2xJ7Azq6hwgTA4RsQUSRT2Mt96Gjsay7AL"),
                Lite.decodePeerId("12D3KooWGndo9bNLTVjrZsT3GJSCwasWVxFieMuT8kjAEPVwKutB")
        );

        for (PeerId peerId : bootstrap) {
            Peeraddrs peeraddrs = Lite.findPeeraddrs(session, peerId, 60);
            if (TestEnv.hasNetwork()) {
                assertFalse(peeraddrs.isEmpty()); // should not be empty
            }
            for (Peeraddr peeraddr : peeraddrs) {
                TestEnv.error("findPeer : " + peeraddr.toString());
            }
        }
    }

    @Test(expected = Exception.class) // can be Timeout or Connect Exception dependent of Network
    public void swarmPeer() throws Exception {

        Lite lite = TestEnv.getTestInstance();


        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        PeerId peerId = Lite.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");

        String host = "139.178.68.146";
        Peeraddr peeraddr = Lite.createPeeraddr(peerId, InetAddress.getByName(host), 4001);
        assertEquals(4001, peeraddr.port());

        assertEquals(peeraddr.peerId(), peerId);

        // peeraddr is just a fiction (will fail)
        Lite.dial(session, peeraddr, Parameters.create(ALPN.libp2p));

    }

}
