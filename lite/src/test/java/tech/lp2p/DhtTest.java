package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.ALPN;
import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Key;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Peeraddrs;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dht.DhtService;

public class DhtTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void providers() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Dummy dummy = new Dummy();
        Session dummySession = dummy.createSession();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Parameters parameters = Parameters.create(ALPN.libp2p);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {

            Cid cid = Lite.rawCid("test");
            Key key = cid.createKey();

            // should return empty list (server does not support GET_PROVIDERS)
            Peeraddrs providers = DhtService.providers(connection, key);
            assertNotNull(providers);
            assertTrue(providers.isEmpty());

        }
    }

    @Test
    public void findPeer() throws Throwable {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        Dummy dummy = new Dummy();

        Session dummySession = dummy.createSession();
        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Parameters parameters = Parameters.create(ALPN.libp2p);

        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            PeerId peerId = TestEnv.random();
            Peeraddrs peeraddrs = DhtService.findPeeraddrs(connection, peerId.createKey());
            assertTrue(peeraddrs.isEmpty()); // peerId not inside the DHT
        }
    }
}
