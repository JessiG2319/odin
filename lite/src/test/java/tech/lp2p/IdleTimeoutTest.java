package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class IdleTimeoutTest {

    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void idleTimeoutTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Dummy dummy = new Dummy();
        Session dummySession = dummy.createSession();

        Parameters parameters = Lite.createParameters();
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
            assertNotNull(envelope);

            // check if connection is connected
            assertTrue(connection.isConnected());

            assertEquals(server.connections(dummy.self()).size(), 1); // also server is alive

        }

        Thread.sleep(3000);
        assertFalse(server.hasConnection(dummy.self()));


    }


    @Test
    public void idleTimeoutWithKeepAliveTest() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);


        Dummy dummy = new Dummy();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session dummySession = dummy.createSession();

        Parameters parameters = Lite.createParameters(true);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
            assertNotNull(envelope);

            // check if connection is connected
            assertTrue(connection.isConnected());

            assertEquals(server.connections(dummy.self()).size(), 1); // also server is alive
        }

        Thread.sleep(3000);
        assertFalse(server.hasConnection(dummy.self()));


    }

}
