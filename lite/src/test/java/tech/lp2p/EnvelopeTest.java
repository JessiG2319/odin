package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Session;

public class EnvelopeTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void envelopeBasic() throws Exception {
        Lite lite = TestEnv.getTestInstance();

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        Cid cid = Lite.rawCid("moin");
        Envelope envelope = Lite.createEnvelope(session, TestEnv.RAW, cid);
        assertNotNull(envelope);
        assertEquals(envelope.peerId(), session.self());

        byte[] transformed = Envelope.toArray(envelope);
        assertNotNull(transformed);

        Envelope cmp = Envelope.toEnvelope(transformed);
        assertNotNull(cmp);

        assertEquals(cmp, envelope);
    }

}
