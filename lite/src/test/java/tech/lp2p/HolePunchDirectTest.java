package tech.lp2p;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Reservation;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;


public class HolePunchDirectTest {


    @BeforeClass
    public static void setup() {

        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void testHolepunch() throws Throwable {

        Lite lite = TestEnv.getTestInstance();

        Server server = TestEnv.getServer();
        assertNotNull(server);


        if (!Lite.hasReservations(server)) {
            TestEnv.error("nothing to test no dialable addresses");
            return;
        }

        Set<Reservation> reservations = Lite.reservations(server);

        AtomicInteger success = new AtomicInteger(0);

        for (Reservation reservation : reservations) {

            PeerId relayID = reservation.peerId();
            assertNotNull(relayID);
            Peeraddr relay = reservation.peeraddr();
            assertNotNull(relay);

            assertTrue(reservation.limitData() > 0);
            assertTrue(reservation.limitDuration() > 0);


            TestEnv.error(reservation.toString());

            Dummy dummy = new Dummy();

            Session dummySession = dummy.createSession();

            try {
                try (Connection connection = Lite.hopConnect(dummySession, reservation.peeraddr(),
                        lite.self(), Lite.createParameters())) {

                    Objects.requireNonNull(connection);

                    Cid rawCid = Lite.rawCid("moin elf");
                    Envelope data = Lite.createEnvelope(dummySession, TestEnv.RAW, rawCid);


                    Lite.pushEnvelope(connection, data);
                    Thread.sleep(2000);

                    Envelope envelope = TestEnv.PUSHED.get();
                    assertArrayEquals(envelope.cid().hash(), rawCid.hash());
                    success.incrementAndGet();
                }
            } catch (Throwable throwable) {
                TestEnv.error("Failed " + throwable.getMessage());
            }
        }
        assertTrue(Lite.hasReservations(server));
        TestEnv.error("Success " + success.get() + " Total " + reservations.size());

        assertTrue(success.get() > 0);
    }
}
