package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;

import junit.framework.TestCase;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import tech.lp2p.core.Connection;
import tech.lp2p.core.Fid;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;

public class FetchStressTest {

    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }

    @Test
    public void stressFetchCalls() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);

        long now = System.currentTimeMillis();

        int dataSize = Lite.CHUNK_DATA;
        Fid fid;
        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);
        fid = TestEnv.createContent(session, "text.bin",
                TestEnv.getRandomBytes(dataSize));


        assertNotNull(fid);

        TestEnv.error("Store Input Stream : " + fid +
                " Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
        now = System.currentTimeMillis();

        PeerId host = lite.self();
        assertNotNull(host);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());


        Dummy dummy = new Dummy();
        Session dummySession = dummy.createSession();
        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(dummySession, peeraddr, parameters)) {
            Objects.requireNonNull(connection);

            long step = System.currentTimeMillis();

            int finalRead = 0;
            for (int i = 0; i < TestEnv.ITERATIONS; i++) {

                AtomicInteger read = new AtomicInteger(0);
                Lite.fetchToOutputStream(connection, new OutputStream() {
                    @Override
                    public void write(int b) {
                        read.incrementAndGet();
                    }
                }, fid.cid(), new TestEnv.DummyProgress());

                TestCase.assertEquals(read.get(), dataSize);
                finalRead += read.get();

                if (Math.floorMod(i, 25) == 0) {
                    TestEnv.error("Read Data Iteration : " + i +
                            " Time : " + ((System.currentTimeMillis() - step)) + "[ms]");
                    step = System.currentTimeMillis();
                }

            }

            TestEnv.error("Read Data : " + finalRead +
                    "[bytes] Time : " + ((System.currentTimeMillis() - now) / 1000) + "[s]");
        }

    }
}
