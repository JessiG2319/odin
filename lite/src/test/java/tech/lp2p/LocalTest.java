package tech.lp2p;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import tech.lp2p.core.Cid;
import tech.lp2p.core.Connection;
import tech.lp2p.core.Envelope;
import tech.lp2p.core.Parameters;
import tech.lp2p.core.PeerId;
import tech.lp2p.core.Peeraddr;
import tech.lp2p.core.Server;
import tech.lp2p.core.Session;
import tech.lp2p.dag.DagService;


public class LocalTest {


    @BeforeClass
    public static void setup() {
        TestEnv.cleanup();
    }

    @AfterClass
    public static void shutdown() {
        TestEnv.cleanup();
    }


    @Test
    public void ownServiceConnect() throws Exception {

        Lite lite = TestEnv.getTestInstance();
        Server server = TestEnv.getServer();
        assertNotNull(server);
        Peeraddr peeraddr = Peeraddr.loopbackPeeraddr(lite.self(), server.port());

        Session session = lite.createSession(TestEnv.getBlockStore(), TestEnv.PEERS);

        Parameters parameters = Lite.createParameters(false);
        try (Connection connection = Lite.dial(session, peeraddr, parameters)) {
            assertNotNull(connection);

            Envelope envelope = Lite.pullEnvelope(connection, TestEnv.CID);
            assertNotNull(envelope);
            PeerId from = envelope.peerId();
            assertEquals(from, lite.self());
            Cid entry = envelope.cid();
            assertNotNull(entry);
            assertEquals(entry, DagService.createEmptyDirectory(""));

        }

        Thread.sleep(3000);
        assertFalse(server.hasConnection(lite.self()));

    }
}
